
<?php
$hidden = array(
	'search' 		=> $this->input->get('search')
);
?>
<?php echo form_open('export/?search=speed', '', $hidden); ?>
    <?php echo form_fieldset(''); ?>
      <div class="row collapse">
        <div class="eight mobile-three columns">
        	<?php 
        	
        	$data_input_search = array(
            'name'        => 'stringsearch',
            'id'          => 'stringsearch',
            'size'		=> '60',
            'placeholder'       => 'Tapez des mots clés : Nom de la société, ville'
          );
    
  	      echo form_input($data_input_search); ?>
  	    </div>
  	    <div class="four mobile-one columns"><?php echo form_submit('submit', 'Rechercher', 'class="postfix button expand"'); ?></div>
	    </div>
    <?php echo form_fieldset_close(); ?>
<?php echo form_close(); ?>    

<?php
$hidden = array();
$hidden = array(
	'query' 		=> @$query
);
?>
<?php echo form_open('export/?search=speed&export=true', '', $hidden); ?>
        <?php echo form_submit('submit', 'Exporter', 'class="right button"'); ?>
<?php echo form_close(); ?>    

<?php if (isset($result)): ?>
  <h5>Total : <?php echo $total; ?></h5>
  <table class="responsive">
    <thead>
      <th>Société</th>
      <th>Type</th>
      <th>Ville</th>
    </thead>
    <tbody>
    <?php foreach ($result -> result() as $value): ?>
      <?php

      // Récupération informations contact
      $link = site_url('fichier/?mode=menu_client&id_company='.$value->id);
      ?>
      <tr>
        <td><a href="<?php echo $link; ?>"><?php echo $value->nom_societe; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->type; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->ville; ?></a></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
<?php endif; ?>