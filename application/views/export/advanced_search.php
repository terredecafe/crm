<?php
$hidden = array(
	'search' 		=> $this->input->get('search')
);
?>
<?php echo form_open('export/?search=advanced', '', $hidden); ?>
    <?php echo form_fieldset(''); ?>
    
    <div class="three columns">
        <label for="ville-fld">Ville</label>
          <?php $data_input_ville = array(
                  'name'        => 'ville',
                  'id'          => 'ville-fld',
                  'size'    => '35',
                  'value'       => @$ville
                );
    
        echo form_input($data_input_ville); ?>
    </div>

    <div class="three columns">
        <label for="ville-fld">Société</label>
        	<?php $data_input_societe = array(
                  'name'        => 'societe',
                  'id'          => 'societe-fld',
                  'size'		=> '35',
                  'value'       => @$societe
                );
    
    		echo form_input($data_input_societe); ?>
    </div>
    
    <div class="three columns">
        <label for="groupe-fld">Groupe</label>
        	<?php $data_input_groupe = array(
                  'name'        => 'groupe',
                  'id'          => 'groupe-fld',
                  'size'		=> '35',
                  'value'       => @$groupe
                );
    
    		echo form_input($data_input_groupe); ?>
    </div>
    
    <div class="three columns">
        <label for="groupe-fld">Marque</label>
        	<?php $data_input_marque = array(
                  'name'        => 'marque',
                  'id'          => 'marque-fld',
                  'size'		=> '35',
                  'value'       => @$marque
                );
          echo form_input($data_input_marque); ?>
          
    </div>
    
   

    <div class="four columns">
        <label for="activite-fld">Activit&eacute;</label>
        	<?php
        	$options_famille = array(
        	              ''  => 'Toutes',
        	            );
        	foreach ($list_famille->result() as $result):
        		$options_famille[$result->id] = $result->nom_famille;
        	endforeach;
        	$type_checked = @$famille_activite;   
          echo form_dropdown('famille_activite', $options_famille, $type_checked, 'id="activite-fld" class="ui-select"'); ?>
          
    </div>

		<div class="two columns">
		    <label for="type-fld">Type</label>
		    		<?php
		    		$options_type = array(
		    					'' =>  'Choisir',
		    		            'Client'  =>  'Client',
		    		            'PR1' 	  =>  'PR1',
		    		            'PR2' 	  =>  'PR2',
		    		          );
		    
		    		$type_checked = @$type;                
		    		echo form_dropdown('type', $options_type, $type_checked, 'id="type-fld" class="ui-select"'); ?>
		      
		</div>

    <div class="three columns">
        <label for="commercial-fld">Commercial</label>
    		<?php
    		$options = array(
            'all'  => 'Tous',
          );
          
    		$i=0;
    		foreach ($list_commercial->result() as $result):
    			$i++;
    			$options[$result->id] = $result->prenom.' '.$result->nom;
    		endforeach;
    		
    		if(@$id_contact)
    			$options_checked = $id_contact;                   
    		else
    			$options_checked = $this->session->userdata('id');
    
    		echo form_dropdown('id_contact', $options, $options_checked, 'id="commercial-fld" class="ui-select"'); ?>
          
    </div>
	  
	  <div class="three columns">
	      <label for="entite-fld">Entité</label>
	  				<?php
	  				$options = array(
	  		                  'all'  	=> 'Toutes les entités',
	  		                  'F1P'	=> 'F1P Lille/Lens/Béthune'
	  		                );
	  				$i=0;
	  				foreach ($list_entite->result() as $result):
	  					$i++;
	  					$options[$result->id] = $result->nom;
	  				endforeach;
	  				
	  				if(@$id_entite)
	  					$options_checked = $id_entite;                   
	  				else
	  					$options_checked = $user->id_entite;
	  		
	  				echo form_dropdown('id_entite', $options, $options_checked, 'id="entite-fld" class="ui-select"'); ?>
	        
	  </div>
	  
	  <div class="four columns">
	      <label for="zone-fld">Zone géographique</label>
  				<?php
  				$options_zones_geo = array(
  		                  ''  => 'Choisir',
  		                );		
  				foreach ($list_zones_geo->result() as $result):
  					$options_zones_geo[$result->id] = $result->value;
  				endforeach;
  		
  				$type_checked = @$id_zone_geo;                
  				echo form_dropdown('id_zone_geo', $options_zones_geo, $type_checked, 'id="zone-fld" class="ui-select"'); ?>
	        
	  </div>

      <div class="five columns">
          <label for="zone-fld">
            <?php
              if (@$email)
                $emailValue = true;
              else
                $emailValue = false;
            	$data = array(
              'name'        => 'email',
              'id'          => 'email',
              'value'       => 'true',
              'checked'     => @$email,
              );

              echo form_checkbox($data);
            ?>
            <span>Seulement les fiches avec une adresse email renseignée</span>
          </label>
                      
      </div>
      <div class="twelve columns">
        <a class="small button" href="<?php echo site_url('export/?search=objection'); ?>" style="font-size:14px;">Export des fiches avec objections
        </a>
        <?php echo form_submit('submit', 'Valider', 'class="right button"'); ?>
      </div>
    <?php echo form_fieldset_close(); ?>
<?php echo form_close(); ?>    

<div class="row">
<?php
$hidden = array();
$hidden = array(
	'query' 		=> @$query
);
?>

<?php if (isset($advanced_result)): ?>
  <?php if ($user->id == '1' OR $user->id == '2'): // Le bouton export uniquement pour Monelle (1) et Malik (2) ?>
    <?php echo form_open('export/?search=speed&export=true', 'id="expoooort"', $hidden); ?>
      <div class="twelve columns">
        <?php echo form_submit('submit', 'Exporter', 'class="right button"'); ?>
      </div>
    <?php echo form_close(); ?> 
  <?php endif; ?>

  <div class="twelve columns">
  <h5>Total : <?php echo $total; ?></h5>
  <table class="responsive">
    <thead>
      <th>Société</th>
      <th>Commercial</th>
      <th>Type</th>
      <th>Ville</th>
    </thead>
    <tbody>
    <?php foreach ($advanced_result -> result() as $value): ?>
      <?php

      // Récupération informations contact
      $link = site_url('fichier/?mode=menu_client&id_company='.$value->id);
      ?>
      <tr>
        <td><a href="<?php echo $link; ?>"><?php echo $value->nom_societe; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->u_prenom.' '.$value->u_nom; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->type; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->ville; ?></a></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    </div>
    <!--
    <?php echo form_open('export/?search=speed&export=true', 'id="export2"', $hidden); ?>
      <div class="twelve columns">
        <?php echo form_submit('submit2', 'Exporter', 'class="right button"'); ?>
      </div>
    <?php echo form_close(); ?> 
    -->
    <?php
    else:
    ?>
      <div class="twelve columns">
        <div class="alert-box"> Faites votre recherche !</div>
      </div>
    <?php
    endif;
    ?>

  </div>