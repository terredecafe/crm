        </div>
        <footer class="colophon">
            <?php
            switch($this->uri->segment(1)):
            	case 'phoning':
            	?>
            	<ul class="nav-bar">
            		<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            		<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
            		<li><a href="javascript:window.print();"><i class="gen-enclosed icon-page"></i>&nbsp;&nbsp;Imprimer</a></li>
            	</ul>
            	<?php
            	break;
            
            	case 'taches':
            	?>
            	<ul class="nav-bar">
            		<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            		<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
            		<li><a href="<?php echo site_url(); ?>taches/?mode=create"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>	
            	</ul>
            	<?php
            	break;	
            
            	case 'docs':
            	?>
            	<ul class="nav-bar">
            		<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            		<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
                        <?php if (empty($_GET['id_entreprise'])): ?>
            		    <li><a href="<?php echo site_url(); ?>docs/?mode=create"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>	
                        <?php else: ?>
                              <li><a href="<?php echo site_url(); ?>docs/?mode=create&id_entreprise=<?php echo $_GET['id_entreprise']; ?>"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>      
                        <?php endif; ?>
                        <?php if (@$_GET['mode'] == 'update'): ?>
            		    <li><a href="<?php echo site_url(); ?>docs/?mode=delete&id=<?php echo $_GET['id']; ?>&id_entreprise=<?php echo $_GET['id_entreprise']; ?>"><i class="gen-enclosed icon-trash"></i>&nbsp;&nbsp;Supprimer</a></li>	
                        <?php endif; ?>
            	</ul>
            	<?php		
            	break;	
            
            	case 'export':
            	?>
            	<ul class="nav-bar">
            		<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            		<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
                        <!--
            		<li><a href="<?php echo site_url(); ?>export/?search=speed&export=true"><i class="gen-enclosed icon-inbox"></i>&nbsp;&nbsp;Exporter</a></li>
                        -->
            	</ul>
            	<?php
            	break;		
            
            	case 'fichier':
            		switch (@$_GET['mode']):
            			case 'menu_client':
            			?>
            		    <ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
                                    <li><a href="<?php echo site_url(); ?>/fichier/?mode=delete&id_company=<?php echo $id_client; ?>" class="delete"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Supprimer cette fiche</a></li>
            			</ul>
            			<?php
            			break;
            
            			case 'fiche_client':
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
                                    <?php if($this->input->get('action') == 'update'): ?>
            				<li><a href="<?php echo site_url(); ?>phoning/?mode=create&id_client=<?php echo @$id_client; ?>&id_contact=<?php echo @$contact_principal->id; ?>"><i class="gen-enclosed icon-phone"></i>&nbsp;&nbsp;Phoning</a></li>
                                    <?php endif; ?>
                                    <li><a href="<?php echo site_url(); ?>/fichier/?mode=delete&id_company=<?php echo $id_client; ?>" class="delete"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Supprimer cette fiche</a></li>
                                    
            			</ul>
            			<?php
            			break;
            
            			case 'list_contacts':
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
            				<li><a href="<?php echo site_url(); ?>fichier/?mode=fiche_contact&action=create&id_client=<?php echo $id_client; ?>"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>
            			</ul>	
            			<?php		
            			break;

            			case 'fiche_contact':
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
            				<?php if(isset($_GET['id_contact'])): ?>
            				<li><a href="<?php echo site_url(); ?>phoning/?mode=create&id_client=<?php echo $id_client; ?>&id_contact=<?php echo $_GET['id_contact']; ?>"><i class="gen-enclosed icon-phone"></i>&nbsp;&nbsp;Phoning</a></li>
            				<?php endif; ?>
                                    <li><a href="<?php echo site_url(); ?>fichier/?mode=list_contacts&id_client=<?php echo $id_client; ?>&id_contact=<?php echo @$_GET['id_contact']; ?>&action=delete"><i class="gen-enclosed icon-trash"></i>&nbsp;&nbsp;Supprimer</a></li>       
            			</ul>
            			<?php		
            			break;
            			
            			case 'historique_rappel':
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
                                    <li><a href="<?php echo site_url(); ?>phoning/?mode=create&id_client=<?php echo @$id_client; ?>&id_contact=<?php echo @$id_contact; ?>"><i class="gen-enclosed icon-phone"></i>&nbsp;&nbsp;Ajouter</a></li>
            			</ul>
            			<?php		
            			break;	
            			
            			case 'docs':
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
            				<li><a href="<?php echo site_url(); ?>docs/?mode=create&id_entreprise=<?php echo $id_client; ?>"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>
            				<li><a href="<?php echo site_url(); ?>fichier/?mode=docs&id="><i class="gen-enclosed icon-trash"></i>&nbsp;&nbsp;Supprimer</a></li>		
            			</ul>
            			<?php			
            			break;	
            			
            			case 'user':
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
            				<li><a href="<?php echo site_url(); ?>docs/?mode=create&id_entreprise=<?php echo $id_client; ?>"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>
            				<li><a href="<?php echo site_url(); ?>fichier/?mode=docs&id="><i class="gen-enclosed icon-trash"></i>&nbsp;&nbsp;Supprimer</a></li>
            			</ul>
            			<?php			
            			break;	
            
            			default :
            			?>
            			<ul class="nav-bar">
            				<li><a href="javascript:history.go(-1)"><i class="gen-enclosed icon-left-arrow"></i>&nbsp;&nbsp;Retour</a></li>
            				<li><a href="<?php echo site_url(); ?>"><i class="gen-enclosed icon-flag"></i>&nbsp;&nbsp;Accueil</a></li>
                                    <li><a href="<?php echo site_url(); ?>fichier/?mode=fiche_client&action=create"><i class="gen-enclosed icon-add-doc"></i>&nbsp;&nbsp;Ajouter</a></li>                                    
                                    <li><a href="<?php echo site_url(); ?>export"><i class="gen-enclosed icon-trash"></i>&nbsp;&nbsp;Exporter</a></li>   		
            			</ul>
            			<?php	
            			break;
            		endswitch;
            	break;
            endswitch;
            ?>
            </nav>
        </footer>
        <div class="extras-footer row">
            <!--
            <div class="columns six">
              <span class="theme-label">Thème</span>
              <ul class="theme-switcher">
                <li><a href="#" rel="default" class="default-theme styleswitch">default</a></li>
              	<li><a href="#" rel="blue" class="blue-theme styleswitch">blue</a></li>
              	<li><a href="#" rel="wood" class="wood-theme styleswitch">wood</a></li>
              	<li><a href="#" rel="purple" class="purple-theme styleswitch">purple</a></li>
              </ul>
            </div>
            -->
            <div class="columns twelve"><img class="right" src="<?php echo site_url('assets/img/crm.png'); ?>" alt="Groupe Force 1" /></div>
        </div>
    </div>
</div>
<script src="<?php echo site_url('assets/js/jquery-1.7.2.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/jquery-ui-1.8.23.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/select2/select2.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/foundation/responsive-tables.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/stacktable.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/global.js'); ?>"></script>
</body>
</html>