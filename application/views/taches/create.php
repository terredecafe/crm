<?php
if ($this->input->get('mode') == 'update'):
  $hidden = array(
      'type' => $this->input->get('mode'),
      'id_tache' => $this->input->get('id')
      );
else:
  $hidden = array(
      'type' => $this->input->get('mode')
      );
endif;


$options_company = array();
foreach ($list_company->result() as $result):
  $options_company[$result->id] = $result->nom_societe;
endforeach;



?>

<?php echo form_open(current_url(), '', $hidden); ?>
    <?php echo form_fieldset(); ?>
    <div class="row">
        <div class="six columns">
            <div>
                <label for="societe-fld">Soci&eacute;t&eacute;</label>
                <?php 
                $societe_checked = @$infos_tache->id_entreprise;      
                echo form_dropdown('company', $options_company, $societe_checked, 'class="ui-select" id="societe-fld"'); ?>
            </div>
            <div>
                 <label for="type-fld">Type</label>
                <?php
                $type_checked = @$infos_tache->type;                
                echo form_dropdown('type_tache', $type_list, $type_checked, 'class="ui-select" id="type-fld"'); ?>
            </div>
            <div>
                <label for="date-fld">Date</label>
                  <?php 
                  if (@isset($phoning_info->date))
                    $NowDate = $phoning_info->date;
                  else
                    $NowDate = date('d/m/Y');

                  $data_input_date = array(
                  'name'        => 'date',
                  'id'          => 'date-fld',
                  'class'       => 'datepicker',
                  'value'       => $NowDate,
                );
            
                echo form_input($data_input_date); ?>
            </div>
            <?php if ($this->input->get('mode') == 'update'): ?>
            <div>
            	<label for="status-fld" class="inline">Statut</label>
            	<?php echo form_dropdown('cloture', array(0 => 'Actif', 1 => 'Clôturé'), @$infos_tache->cloture, 'data-highlight="true" class="toggleswitch inline" id="statut-fld"'); ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="six columns">
            <label for="commentaires-fld">Commentaires</label> 
            <?php $data_textarea = array(
                    'name'        => 'commentaires',
                    'id'          => 'commentaires-fld',
                    'value'       => @$infos_tache->commentaire,
                    'maxlength'   => '100',
                    'size'        => '50',
                  );
              echo form_textarea($data_textarea); ?>
        </div>
     </div>
    <?php echo form_fieldset_close(); ?>
   <?php echo form_submit('submit', 'Valider', 'class="right button"'); ?>
<?php echo form_close(); ?>    