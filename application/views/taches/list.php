<table class="responsive">
    <thead>
        <th>Date</th>
        <th>Société</th>
        <th>Tâche</th>
        <th>Commentaire</th>
        <th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($results->result() as $value): ?>
    <?php
    	// Gestion du status
    	$status = 'En attente';
    	if ($value->cloture)
    		$status = 'Clotur&eacute;';
    
    	$date = date_us_to_fr($value->date);
    	
    	$link = site_url('taches/?mode=update&id='.$value->id);
        $cloturelink = $link.'&cloture=true';
    ?>
    <tr>
        <td><a href="<?php echo $link; ?>"><?php echo $date; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->nom_societe; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->type; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->commentaire; ?></a></td>
        <td><a href="<?php echo $cloturelink; ?>"><span class="label">Cloturer</span></a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>