<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>CRM 2012</title>
	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!--

	<script>
		//reset type=date inputs to text
		$( document ).bind( "mobileinit", function(){
			$.mobile.page.prototype.options.degradeInputs.date = true;
		});	
	</script>

	-->
	
	<link rel="stylesheet"  href="<?php echo site_url('assets/js/select2/select2.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/wood.css'); ?>" title="default" media="screen" />
    <link rel="stylesheet"  href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" />	
    <?php if((@$_GET['mode'] == 'fiche_client' AND @$_GET['action'] != 'create') OR (@$_GET['mode'] == 'fiche_contact' AND @$_GET['action'] != 'create')): ?>
    <script type="text/javascript">
    var pouet = false;
    window.onbeforeunload = function (){
        if ( pouet == false){
            return false;
        }
    }
    function goToUrl(url){
        pouet = true;
        document.location.href = url;
        return false;
    }
    </script>
    <?php endif; ?>

</head>
<body  data-websiteurl="<?php echo site_url(); ?>" class="<?php echo body_class(); ?>">
<div id="wrapper">
    <div id="cell">
        <header id="branding" class="row">
            <?php if (user_logged_in()): ?>
            <!-- Logo de l'entité -->
            <a href="<?php echo site_url(); ?>">                
                <div class="logo columns eight">
                	<img src="<?php echo @$logo_entite; ?>" alt="" />
                </div>
            </a>
            <div class="columns four">
                <div class="panel">
                    <p><?php if (@$welcome) echo $welcome; ?> </p>
                    <a class="button" href="<?php echo site_url(); ?>user/logout"><i class="gen-enclosed icon-unlock"></i>&nbsp;&nbsp;D&eacute;connexion</a>
                </div>
            </div>
            <?php else: ?>
            <h1><img alt="CRM" src="<?php echo site_url('assets/img/gf1.png'); ?>" /></h1>
            <?php endif; ?>
        </header>
        <div id="main" class="row" data-role="page">
            <?php if(@$header_msg):?>
            <h1><?php echo $header_msg; ?></h1>
            <?php endif; ?>