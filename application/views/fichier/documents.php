<?php


$hidden = array(
    'filters' => 'true',
    );
$attributes = array('id' => 'filters');    
echo form_open(current_url(), $attributes, $hidden);
?>
  <div class="select-sort row">
    <div class="eight mobile-two columns">
      <div class="row collapse">
        <label>Trier par</label>
      </div>
    </div>
    <div class="two mobile-one columns">
      <div class="row collapse">
      <?php
      $option_type = array();

      $options_type = $type_list;
      $options_type['all'] = 'Tous';
      $js = 'id="type_filter"';            
      $type_checked = @$selected_filter;  
      echo form_dropdown('type_filter', $options_type, $type_checked, $js); ?>
      </div>
    </div>

  </div>
<?php echo form_close(); ?>  


<table class="responsive">
    <thead>
        <th>Date</th>
        <th>Société</th>
        <th>Type</th>
        <th>Nom</th>
        <th>Télécharger</th>
    </thead>
    <tbody>
    <?php
    foreach ($docs -> result() as $result):
    ?>
        <tr>
        <?php
        // Récupération informations 
        $Societe = $result->nom_societe;
        $Nom = $result->name;
        $Type = $result->type;
        $Date = date_us_to_fr($result->date);
        // Download Link
        $downloadlink = site_url('uploads/'.@$result->file);
        $updatelink   = site_url('docs/?mode=update&id='.$result->id);
        ?>
        <td><a href="<?php echo $updatelink; ?>"><?php echo $Date; ?></a></td>
        <td><a href="<?php echo $updatelink; ?>"><?php echo $Societe; ?></a></td>
        <td><a href="<?php echo $updatelink; ?>"><?php echo $Type; ?></a></td>
        <td><a href="<?php echo $updatelink; ?>"><?php echo $Nom; ?></a></td>
        <td><a target="_blank" href="<?php echo $downloadlink; ?>"><i class="table-action icon-edit"></i></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>