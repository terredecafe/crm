<?php
if ($this->input->get('action') == 'update'):
	$hidden = array(
			'page'      => 'fiche_client',
			'action' 	=> 'update',
			'id_client' => @$client->id
			);
else:
	$hidden = array(
			'action' => $this->input->get('action'),
			'page'      => 'fiche_client'
			);
endif;

// Si le commercial est admin (créateur de la fiche) il peut modifier, sinon non !
if ($IsAdmin)
    $Action = current_url();
else
    $Action = false;
?>


<?php echo form_open($Action, '', $hidden); ?>

    <?php echo form_fieldset(); ?>
        <div class="row">
            <?php $Verify = @(array)$WhoIs; ?>
            <?php if (@$IsExist): ?>
                <div class="alert-box">
                    ATTENTION : une fiche existe avec le même nom de société !<br />
                    Vous pouvez le modifier directement ci-dessous, la <a href="<?php echo site_url(); ?>/fichier/?mode=delete&id_company=<?php echo $_GET['id_client']; ?>" class="delete">supprimer</a> ou ne rien changer.<br /><br />
                    <p><u>Liste des sociétés existantes :</u></p>
                    <ul>
                        <?php 
                        foreach($WhoIs as $value):
                                $url = site_url().'/fichier/?mode=menu_client&id_company='.$value->id;
                                echo '<a style="color:white;" href="'.$url.'">';
                                echo '<li>'.$value->nom_societe.' à '.$value->ville.' ('.$value->prenom.' '.$value->nom.')</li>';
                        endforeach; 
                        ?>
                    </ul>
                </div>
            <?php endif; ?>
            
            <div class="four columns">
                <label for="nom_societe-fld">Société</label>
                <?php $data_input_societe = array(
                        'name'        => 'nom_societe',
                        'id'          => 'nom_societe-fld',
                        'value'       => @$client->nom_societe,
                      );
            
                echo form_input($data_input_societe); ?>
            </div>

            <div class="two columns">
                <label for="type-fld">Type</label>
                <?php
                $options_type = array(
                            'PR2'    =>  'PR2',
                            'Client'  =>  'Client',
                            'PR1'     =>  'PR1',
                          );
            
                $type_checked = @$client->type;                
                echo form_dropdown('type', $options_type, $type_checked, ' id="type-fld" class="ui-select"'); ?>
            </div>            


            <div class="three columns">
                <label for="id_contact-fld">Commercial</label>
                <?php
                $options_commercial = array();
                foreach ($list_commercial->result() as $result):
                    $options_commercial[$result->id] = $result->prenom.' '.$result->nom;
                endforeach;
            
                $commercial_checked = @$client->id_user;                
                echo form_dropdown('id_contact', $options_commercial, $commercial_checked, ' id="id_contact-fld" class="ui-select"'); ?>
            </div>            


            <div class="three columns">
            	<label for="id_entite-fld">Entité</label>
            	<?php 
            	$options_entite = array();
            	foreach ($list_entite->result() as $result):
            	  $options_entite[$result->id] = $result->nom;
            	endforeach;
            
            	$entite_checked = @$client->id_entite;      
                echo form_dropdown('id_entite', $options_entite, $entite_checked, ' id="id_entite-fld" class="ui-select"'); ?>
            </div>


            <div class="four columns">
            	<label for="famille_activite-fld">Famille d'activité</label>
                <?php
                $options_famille = array();
                foreach ($list_famille->result() as $result):
                    $options_famille[$result->id] = $result->nom_famille;
                endforeach;
            
                $famille_checked = @$client->famille_activite; 
                echo form_dropdown('famille_activite', $options_famille, $famille_checked, ' id="famille_activite-fld" class="ui-select"'); ?>





            </div>
            <div class="four columns">
        	    <label for="nom_groupe-fld">Groupe</label>
            	<?php $data_input_groupe = array(
            	        'name'        => 'nom_groupe',
            	        'id'          => 'nom_groupe-fld',
            	        'value'       => @$client->nom_groupe,
            	      );
            
            	echo form_input($data_input_groupe); ?>
            </div>
            <div class="four columns">
            	<label for="marque-fld">Marque</label>
            	<?php $data_input_marque = array(
            	        'name'        => 'marque',
            	        'id'          => 'marque-fld',
            	        'value'       => @$client->marque,
            	      );
            
            	echo form_input($data_input_marque); ?>
            </div>
            <?php if($entite->id == 1 OR $entite->id == 14): ?>
            <div class="six columns">
                <label for="site_internet-fld">Site web (<a href="http://<?=@$client->site_internet?>">Aller sur le site</a>)</label>
                <?php $data_input_site = array(
                        'name'        => 'site_internet',
                        'id'          => 'site_internet-fld',
                        'placeholder' => 'ex: http://www.terredecafe.fr',
                        'value'       => @$client->site_internet,
                      );
            
                echo form_input($data_input_site); ?>
            </div>
            <div class="six columns">
                <label for="id_qualif-fld">Qualification</label>
                <?php
                $options_qualif = array();
                foreach ($list_qualif->result() as $result):
                    $options_qualif[$result->id] = $result->value;
                endforeach;
            
                $type_checked = @$client->id_qualif;                
                echo form_dropdown('id_qualif', $options_qualif, $type_checked, ' id="id_qualif-fld" class="ui-select"'); ?>
            </div>  
            <div class="six columns">
                <label for="id_tranche_effectif-fld">Effectif</label>
                <?php
                $options_effectif = array();
                foreach ($list_effectif->result() as $result):
                    $options_effectif[$result->id] = $result->value;
                endforeach;
            
                $type_checked = @$client->id_tranche_effectif;                
                echo form_dropdown('id_tranche_effectif', $options_effectif, $type_checked, ' id="id_tranche_effectif-fld" class="ui-select"'); ?>
            </div>

            <div class="six columns">
                <label for="siret-fld">SIRET</label>
                <?php $data_input_siret = array(
                        'name'        => 'siret',
                        'id'          => 'siret-fld',
                        'value'       => @$client->siret,
                      );
            
                echo form_input($data_input_siret); ?>
            </div>
            <div class="six columns">        
                <label for="siren-fld">SIREN</label>
                <?php $data_input_siren = array(
                        'name'        => 'siren',
                        'id'          => 'siren-fld',
                        'value'       => @$client->siren,
                      );
            
                echo form_input($data_input_siren); ?>
            </div>
            <div class="six columns">
                <label for="naf-fld">NAF</label>
                <?php $data_input_naf = array(
                        'name'        => 'naf',
                        'id'          => 'naf-fld',
                        'value'       => @$client->naf,
                      );
            
                echo form_input($data_input_naf); ?>
            </div>            
            <?php endif; ?>          
        </div>
        <?php echo form_fieldset_close(); ?>
        <?php echo form_fieldset(); ?>
        <div class="row">
            <div class="six columns">
            	<label>Adresse</label>
            	<?php $data_input_adresse = array(
            	      'name'        => 'adresse',
            	      'id'          => 'adresse',
            	      'value'       => @$client->adresse,
            	      'class'       => 'twelve',
            	      'placeholder' => 'Rue'
            	    );
            	echo form_input($data_input_adresse); ?>
            </div>
            <div class="six columns">
                <label>Complément d'adresse</label>
                <?php $data_input_complement_adresse = array(
                      'name'        => 'complement_adresse',
                      'id'          => 'complement_adresse',
                      'value'       => @$client->complement_adresse,
                      'class'       => 'twelve',
                      'placeholder' => ''
                    );
                echo form_input($data_input_complement_adresse); ?>
            </div>
        </div>
        <div class="row">
        	<div class="four columns">
        	    	<?php
        		      	$data_input_cp = array(
        	    	        'name'        => 'cp',
        	    	        'id'          => 'cp-fld',
        	    	        'value'       => @$client->cp,
        	    	        'placeholder' => 'Code postal'
        	    	      );
        	    
        		      	echo form_input($data_input_cp);
        	    	?>
        	</div>
    	    <div class="four columns">
            <?php
                $data_input_ville = array(
                    'name'        => 'ville',
                    'id'          => 'ville-fld',
                    'value'       => @$client->ville,
                    'placeholder' => 'Ville'
                );
                
                echo form_input($data_input_ville);
            ?>
    	   </div>
    	   <div class="four columns">
    	  	<label for="id_zone_geo-fld" class="hidden">Zone géographique</label>
    	  	<?php
    	  	$options_zones_geo = array();
            $options_zones_geo[''] = '';
    	  	foreach ($list_zones_geo->result() as $result):
    	  		$options_zones_geo[$result->id] = $result->value;
    	  	endforeach;
    	  
    	  	$zone_checked = @$client->id_zone_geo;                
    	  	echo form_dropdown('id_zone_geo', $options_zones_geo, $zone_checked, ' id="id_zone_geo-fld" class="ui-select" data-placeholder="Zone géographique"'); ?>
    	   </div>
        </div>     
        <div class="row">       
            <div class="six columns">
            	<label for="telephone-fld">
                    <?php if (@isset($client->telephone)) : ?>
                        <a href="tel:<?=@$client->telephone?>">Téléphone</a>
                    <?php else: ?>
                        Téléphone
                    <?php endif; ?>
                </label>
            	<?php $data_input_telephone = array(
            	        'name'        => 'telephone',
            	        'id'          => 'telephone-fld',
            	        'value'       => @$client->telephone,
            	      );
            
            	echo form_input($data_input_telephone); ?>
            </div>
            <div class="six columns">
            	<label for="fax-fld">Fax</label>
            	<?php $data_input_fax = array(
            	        'name'        => 'fax',
            	        'id'          => 'fax-fld',
            	        'value'       => @$client->fax,
            	      );
            
            	echo form_input($data_input_fax); ?>
            </div>
        </div>
        <?php echo form_fieldset_close(); ?>
        <?php echo form_fieldset(); ?>
        <div class="row">

        	<div class="twelve columns">
            	<label for="comments-fld">Commentaires</label>
            	<?php $data_input_comments = array(
            	      'name'        => 'comments',
            	      'id'          => 'comments-fld',
            	      'placeholder' => 'Votre texte ici',
            	      'value'       => @$client->comments,
            	    );
            	echo form_textarea($data_input_comments); ?>
        	</div>
        </div>
    <?php echo form_fieldset_close(); ?>

    <?php if ($IsAdmin): ?>   

    <?php echo form_submit('submit', 'VALIDER', ' class="button right"'); ?>
 
    <?php endif; ?>

<?php echo form_close(); ?>