<ul class="menu-items">
	<?php if(@$no_first_contact): ?>
		<div class="alert-box">
			ATTENTION : Cette fiche ne possède pas de contact principal !<br />
			Pour ajouter un contact, cliquez 
			<a href="<?php echo site_url(); ?>fichier/?mode=fiche_contact&action=create&id_client=<?php echo $id_client; ?>&first=true">ici</a> 
		</div>
	<?php endif; ?>
	<?php if(@$IsBirthday): ?>
		<?php foreach($IsBirthday as $value): ?>
			<div class="alert-box">
				C'est l'anniversaire de 
				<a href="<?php echo site_url() ?>/fichier/?mode=fiche_contact&action=update&id_client=1&id_contact=<?php echo $value->id; ?>">
					<?php echo $value->prenom ?> <?php echo $value->nom ?>
				</a> !
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<li class="two mobile-two columns"><a href="<?php echo current_url(); ?>/?mode=fiche_client&action=update&id_client=<?php echo $id_client; ?>"><i class="icon-page"></i>&nbsp;&nbsp;Fiche client</a></li>
	<li class="two mobile-two columns"><a href="<?php echo current_url(); ?>/?mode=list_contacts&id_client=<?php echo $id_client; ?>"><i class="icon-address-book"></i>&nbsp;&nbsp;Liste contacts</a></li>

	<?php if(!@$no_first_contact): ?>
		<li class="two mobile-two columns"><a href="<?php echo site_url(); ?>phoning/?mode=create&id_client=<?php echo $id_client; ?>&id_contact=<?php echo $id_contact; ?>"><i class="icon-phone"></i>&nbsp;&nbsp;Créer phoning</a></li>
	<?php endif; ?>



	<li class="two mobile-two columns"><a href="<?php echo site_url(); ?>docs/?id_entreprise=<?php echo $id_client; ?>"><i class="icon-folder"></i>&nbsp;&nbsp;Documents</a></li>
	<li class="two mobile-two columns"><a href="<?php echo current_url(); ?>?mode=historique_rappel&id_client=<?php echo $id_client; ?>"><i class="icon-clock"></i>&nbsp;&nbsp;Historique appels</a></li>
	<li class="two mobile-two columns"><a href="<?php echo current_url(); ?>?mode=historique_rdv&id_client=<?php echo $id_client; ?>"><i class="icon-calendar"></i>&nbsp;&nbsp;Historique RDV</a></li>
</ul>