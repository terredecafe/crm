<table class="stack responsive">
    <thead>
        <th>Nom / Prénom</th>
        <th>Téléphone</th>
        <th>Portable</th>
    </thead>
    <tbody>
    <?php foreach ($list_contacts -> result() as $result): ?>
        <tr>
        <?php
            $link = site_url('fichier/?mode=fiche_contact&action=update&id_client='.$id_client.'&id_contact='.$result->id);
        ?>
            <td>
                <a href="<?php echo $link; ?>">
                    <?php echo $result->nom; ?> <?php echo $result->prenom; ?>
                    <?php if ($result->principal): ?>&nbsp;<i class="table-action icon-star"></i><?php endif; ?>
                </a>
            </td>
            <td><a href="tel:<?php echo $result->telephone; ?>"><?php echo $result->telephone; ?></a></td>
            <td><a href="tel:<?php echo $result->gsm; ?>"><?php echo $result->gsm; ?></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>