<table class="responsive">
    <thead>
        <th>Date création</th>
        <th>Contact</th>
        <th>Résultat</th>
        <th>Date RAPPEL/RDV</th>
        <th>Commentaires</th>
        <th>Objection</th>
    </thead>
    <tbody>
    <?php
    foreach ($historique->result() as $result):
    ?>
        <tr>
        <?php
            // Récupération informations contact
            $Contact = $result->c_prenom . ' ' . $result->c_nom;
            
            // Gestion du status
        	if ($result->t_result_rdv): // Si c'est un RDV
        		$Type = "RDV";
                $TheDate = @date_us_to_fr_with_time($result->t_result_rdv_date);
        	else: // Sinon, c'est un Rappel
        		$Type = "Rappel";
                $TheDate = @date_us_to_fr($result->t_result_rappel_date);
            endif;
        
        		if ($result->t_objection) // S'il y a objection'
        		$Objection = "Objection !";
        	else // Sinon, 
        		$Objection = "";
        
            $date = date_us_to_fr($result->t_date);
            $link = site_url('phoning/?mode=update&id='.$result->t_id.'&id_contact='.$result->c_id);
        ?>
            <td><a href="<?php echo $link; ?>"><?php echo $date; ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo $Contact; ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo $Type; ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo $TheDate; ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo substr($result->t_commentaire, 0, 120); ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo $Objection; ?></a></td>        
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>