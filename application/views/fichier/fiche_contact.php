<?php
if ($this->input->get('action') == 'update'):
	$hidden = array(
			'page'      => 'fiche_contact',
			'action' 	=> 'update',
			'id_contact' => @$contact_info->id,
			'id_entreprise' => @$contact_info->id_entreprise
			);
else:
	$hidden = array(
			'action' => $this->input->get('action'),
			'page' => 'fiche_contact',
			'id_entreprise' => @$this->input->get('id_client')
			);
endif;
?>


<?php echo form_open(current_url(), '', $hidden); ?>
    <?php echo form_fieldset('', 'class="fiche-contact-form"'); ?>
    <div class="row">
        <div class="four columns">
            <label>Civilité</label>
            
            <label class="inline" for="civilite-fld-mme">
                <?php if (@$contact_info->civilite == 'Mme') $Mme = true; else $Mme = false; ?>
                <?php echo form_radio('civilite', 'Mme', @$Mme, 'id="civilite-fld-mme"'); ?>
                Mme
            </label>
            <label class="inline" for="civilite-fld-mlle">
                <?php if (@$contact_info->civilite == 'Mlle') $Mlle = true; else $Mlle = false; ?>
                <?php echo form_radio('civilite', 'Mlle', @$Mlle, 'id="civilite-fld-mlle"'); ?>
                Mlle
            </label>
            <label class="inline" for="civilite-fld-m">
                <?php if (@$contact_info->civilite == 'M') $M = true; else $M = false; ?>
                <?php echo form_radio('civilite', 'M', @$M, 'id="civilite-fld-m"'); ?>
                M
            </label>
        </div>
        <div class="four columns">
        	<label for="nom-fld">Nom</label>
        	<?php $data_input_nom = array(
        	        'name'        => 'nom',
        	        'id'          => 'nom-fld',
        	        'value'       => @$contact_info->nom,
        	      );
        	echo form_input($data_input_nom); ?>
        </div>
        <div class="four columns">
        	<label for="prenom-fld">Prénom</label>
        	<?php $data_input_nom = array(
        	        'name'        => 'prenom',
        	        'id'          => 'prenom-fld',
        	        'value'       => @$contact_info->prenom,
        	      );
        
        	echo form_input($data_input_nom); ?>
        </div>
        <div class="star-contact">
            <label class="right" for="principal-fld">
                <?php
                if (@$first_contact)
                        echo form_checkbox('principal', 'true', 'id="principal-fld"');
                else
                        echo form_checkbox('principal', 'true', @$contact_info->principal,  'id="principal-fld"');
                ?>
                <div>
                    <i class="table-action icon-star"></i>
                    Contact principal
                </div>
            </label>
	    </div>
        <div class="four columns">
        	<label for="function-fld">Fonction</label>
        	<?php 
        	$options_fonctions = array();
        	foreach ($list_fonctions->result() as $result):
        	  $options_fonctions[$result->value] = $result->value;
        	endforeach;
        	$fonction_checked = @$contact_info->fonction;      
            echo form_dropdown('fonction', $options_fonctions, $fonction_checked, ' class="ui-select" id="function-fld"'); ?>
        </div>
        <div class="four columns">
        	<label for="telephone-fld"><a href="tel:<?=@$contact_info->telephone?>">Téléphone</a></label>

        	<?php $data_input_nom = array(
        	        'name'        => 'telephone',
        	        'id'          => 'telephone-fld',
        	        'value'       => @$contact_info->telephone,
        	      );
        
        	echo form_input($data_input_nom); ?>
        </div>
        <div class="four columns">
        	<label for="gsm-fld"><a href="tel:<?=@$contact_info->gsm?>">Mobile</a></label>
        	<?php $data_input_nom = array(
        	        'name'        => 'gsm',
        	        'id'          => 'gsm-fld',
        	        'value'       => @$contact_info->gsm,
        	      );
        
        	echo form_input($data_input_nom); ?>
        </div>
        <div class="four columns">
        	<label for="email-fld"><a href="mailto:<?=@$contact_info->email?>">Email</a></label>
        	<?php $data_input_nom = array(
        	        'name'        => 'email',
        	        'id'          => 'email-fld',
        	        'value'       => @$contact_info->email,
        	      );
        
        	echo form_input($data_input_nom); ?>
        </div>
        <div class="four columns">
            <label for="birthday-fld">Anniversaire
            <?php
            if (@$contact_info->birthday == '')
                echo ' <strong style="color:red;">(à renseigner)</strong>';
            ?>
            </label>
        	<?php $data_input_bday = array(
        	        'name'        => 'birthday',
        	        'id'          => 'birthday-fld',
        	        'class'       => 'datepicker',
        	        'value'       => @$contact_info->birthday,
        	      );
        
        	echo form_input($data_input_bday); ?>
        </div>
        <div class="four columns">
        	<label for="viadeo-fld">Viadeo</label>
        	<?php $data_input_bday = array(
        	        'name'        => 'viadeo',
        	        'id'          => 'viadeo-fld',
        	        'value'       => @$contact_info->viadeo,
        	      );
        
        	echo form_input($data_input_bday); ?>
        </div>
    </div>
    <?php echo form_fieldset_close(); ?>
    <?php echo form_submit('submit', 'Valider', 'class="right button"'); ?>
<?php echo form_close(); ?>