<?php echo form_open(current_url().'/?mode=menu_client', '', ''); ?>
    <fieldset>
    <p>
        <!--<input type="hidden" name="entreprise" class="select2 bigdrop" id="entreprise" style="width:100%" value=""/>-->
        <?php
        $options_list = array();
        $temp = $userlist;
        foreach ($temp as $result):
            if (empty($contact_list[$result->id]))
                $Contact = '';
            else
                $Contact = '('.$contact_list[$result->id].')';

            $options_list[$result->id] = $result->nom_societe . ' ' . $result->ville . ' '.$Contact;
            //$options_list[$result->id] = $result->nom_societe . ' ' . $result->ville ;
        endforeach;

        $type_checked = '';         
        echo form_dropdown('entreprise', $options_list, $type_checked, ' id="famille_activite-fld" class="ui-select"'); 
        ?>
    </p>
    <p class="right">
        <?php
        echo form_button(
            array(
                'name' => 'submit',
                'value' => 'login',
                'id' => 'submit-btn',
                'type' => 'submit',
                'content' => 'Voir la fiche',
                'class' => 'small button',
            )
        );
        ?>
    </p>
    </fieldset>
<?php echo form_close(); ?>
<p align="left">
    <a href="<?php echo site_url('export/?search=advanced'); ?>">
        <button id="submit-btn" class="small button" value="login" type="submit" name="submit" style="font-size:20px;">Effectuer une recherche avancée</button>
    </a>
    
</p>
<h5>Total : <?php echo $usercount[0]->total; ?></h5>
<?php
$hidden = array();
$hidden = array(
    'query'         => @str_replace(' COUNT(*) as total, e.id, ', ' ', $query)
);
?>

<?php echo form_open('export/?search=speed&export=true', '', $hidden); ?>
        <?php echo form_submit('submit', 'Exporter', 'class="right button"'); ?>
<?php echo form_close(); ?>  

<table class="responsive">
    <thead>
        <th>Société</th>
        <th>Type</th>
        <th>Ville</th>
    </thead>
    <tbody>
    <?php foreach ($userlist as $value): ?>
    <?php

    // Récupération informations contact
    $link = site_url('fichier/?mode=menu_client&id_company='.$value->id);
    ?>
    <tr>
        <td><a href="<?php echo $link; ?>"><?php echo $value->nom_societe; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->type; ?></a></td>
        <td><a href="<?php echo $link; ?>"><?php echo $value->ville; ?></a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<!--
<?php echo form_open('export/?search=speed&export=true', '', $hidden); ?>
        <?php echo form_submit('submit', 'Exporter', 'class="right button"'); ?>
<?php echo form_close(); ?> 
-->  