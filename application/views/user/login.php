<?php echo $this->session->flashdata('validation_msg'); ?>
<?php if ($errors): ?>
<div class="alert-box alert"><?php echo $errors; ?></div>
<?php endif; ?>
<?php echo form_open(site_url('user/proceed')); ?>
    <?php echo form_fieldset('Vos informations de connexion'); ?>
        <div class="row collapse">
            <div class="two mobile-one columns">
                <span class="prefix"><i class="gen-enclosed icon-mail"></i></span>
            </div>
             <div class="ten mobile-three columns">
            <?php
            echo form_input(
                array(
                    'name' => 'email',
                    'id' => 'email-fld',
                    'value' => $email,
                    //'type' => 'email',
                    'placeholder' => 'Votre prénom',
                )
            );
            ?>
            </div>
        </div>
        <div class="row collapse">
        <div class="two mobile-one columns">
            <span class="prefix"><i class="gen-enclosed access-icon-key"></i></span>
        </div>
         <div class="ten mobile-three columns">
            <?php
            echo form_password(
                array(
                    'name' => 'password',
                    'id' => 'password-fld',
                )
            );
            ?>
        </div>
        </div>
        <div class="right">
        <?php
        echo form_button(
            array(
                'name' => 'submit',
                'value' => 'login',
                'id' => 'submit-btn',
                'type' => 'submit',
                'content' => '<i class="gen-enclosed icon-lock"></i>&nbsp;&nbsp;S\'authentifier',
                'class' => 'small button',
            )
        );
        ?>
        </div>
        <?php
            echo form_hidden('url', site_url('user'));
        ?>
    <?php echo form_fieldset_close(); ?>
<?php echo form_close(); ?>