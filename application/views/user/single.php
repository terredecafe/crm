
<div class="fete"></div>
	<?php if(@$bday): ?>
		<?php foreach($bday as $value): ?>
			<div class="alert-box" style="font-size: 16px;">
				[Birthday Alert] C'est l'anniversaire de 
				<a style="color: white;" href="<?php echo site_url() ?>/fichier/?mode=fiche_contact&action=update&id_client=<?php echo $value['id_client']; ?>&id_contact=<?php echo $value['id_contact']; ?>">
					<u><?php echo $value['prenom']; ?> <?php echo $value['nom']; ?> (<?php echo $value['nom_societe']; ?>)</u>
				</a> !
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
<ul class="menu-items">
	<li class="three mobile-two columns">
	    <a href="<?php echo site_url(); ?>fichier"><i class="gen-enclosed icon-address-book"></i> Mon Fichier</a>
	</li>
	<li class="three mobile-two columns">
	    <a href="<?php echo site_url(); ?>phoning"><i class="gen-enclosed icon-phone"></i> Mon Phoning</a>
	</li>
	<li class="three mobile-two columns">
	    <a href="<?php echo site_url(); ?>taches"><i class="gen-enclosed icon-checkmark"></i> Mes Tâches</a>
	</li>
	<!--
	<li class="four mobile-two columns">
	    <a href="<?php echo site_url(); ?>docs"><i class="gen-enclosed icon-folder"></i> Mes Documents</a>
	</li>
	-->
	<li class="three mobile-two columns">
	    <a target="_blank" href="https://www.google.com/calendar/render?hl=fr"><i class="gen-enclosed icon-calendar"></i> Mon planning</a>
	</li>
	<!--
	<li class="four mobile-two columns">
	    <a href="<?php echo site_url(); ?>export"><i class="gen-enclosed icon-inbox"></i> Exporter</a>
	</li>
	-->
</ul>