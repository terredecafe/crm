<?php
if ($this->input->get('mode') == 'update'):
	$hidden = array(
			'type' => $this->input->get('mode'),
			'id_phoning' => $phoning_info->id,
      'id_contact' => $phoning_info->id_contact,
      'id_entreprise' => $phoning_info->id_entreprise
			);
else:
	$hidden = array(
			'type' => $this->input->get('mode'),
      'id_contact' => $this->input->get('id_contact'),
      'id_entreprise' => $this->input->get('id_client')      
			);
endif;
?>
<?php echo form_open(current_url(), '', $hidden); ?>
    <?php echo form_fieldset(''); ?>
    <p>
        <a href="https://accounts.google.com/o/oauth2/auth?response_type=code&amp;redirect_uri=https%3A%2F%2Fapp.terredecafe.fr%2Fapi.google%2Faccueil.php&amp;client_id=47786876072.apps.googleusercontent.com&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar.readonly&amp;access_type=offline&amp;approval_prompt=force" class="login" target="_blank"><strong>OBLIGATOIRE : </strong>Se connecter et autoriser Google pour la synchronisation Agenda</a>
    </p>
    <div class="row">
        <div class="six columns">
            <div>

        	</div>
        	<div>
        	    <label for="type-fld">Type</label>
        		<?php
        		$options_resultat = array(
        	              'rdv'    =>  'RDV positionn&eacute;',
        	              'rappel' =>  'Rappel',
        	            );
        		echo form_dropdown('resultat', $options_resultat, @$phoning_info->resultat, 'class="ui-select" id="type-fld"'); ?>
        	</div>
        	<div>
    	        <label for="objection-fld">Objection</label>
    	        <?php
    	        $options_resultat = array(
    	                      '0' =>  'Pas d\'objection',
    	                      '1' =>  'Objection au rendez-vous',
    	                    );
    	        echo form_dropdown('objection', $options_resultat, @$phoning_info->objection, 'class="ui-select" id="objection-fld"'); ?>
        	</div>
            <label for="status-fld" class="inline"><strong>Statut</strong></label>
            <?php $Cloture = array(0 => 'Actif', 1 => 'Clôturé'); ?> 
            <?php echo form_dropdown('cloture', $Cloture , @$phoning_info->cloture, 'data-highlight="true" class="toggleswitch inline" id="statut-fld"'); ?>
        
        </div>
        <div class="six columns">
          	<div>
                <!--<label for="date-fld">Date de création de la tâche</label>On le cache-->
                
                <div>
                <label for="date_rdv_rappel-fld">Date <span class="rdv-content">du rendez-vous</span> <span class="rappel-content">du rappel</span></label>
                <?php $data_input_date = array(
                      'name'        => 'date_rdv_rappel',
                      'id'          => 'date_rdv_rappel-fld',
                      'class'       => 'datepicker',
                      'value'       => @$phoning_info->resultat_date,
                    );
                echo form_input($data_input_date); ?>
            </div>
            <div>
                <label for="time_rdv_rappel-fld">Heure <span class="rdv-content">du rendez-vous</span> <span class="rappel-content">du rappel</span>
                <strong>IMPORTANT - HH:MM (ex: 10:00)</strong></label>
                <?php $data_input_time = array(
                      'name'        => 'time_rdv_rappel',
                      'id'          => 'time_rdv_rappel-fld',
                      'value'       => @$phoning_info->time_rdv_rappel,
                    );
        
                echo form_input($data_input_time); ?>

            </div>
              	<label for="commentaires-fld">Commentaires</label>
              	<?php $data_textarea = array(
                        'name'        => 'commentaires',
                        'id'          => 'commentaires-fld',
                        'value'       => @$phoning_info->commentaire,
                        'size'        => '50',
                      );
                echo form_textarea($data_textarea); ?>
                <?php echo mailto(@$infos_contact->email, '<i class="gen-enclosed icon-mail"></i>&nbsp;&nbsp;Cliquez ici pour envoyer un email', 'class="right button"'); ?>
            </div>
            <?php
                if (@isset($phoning_info->date))
                    $NowDate = $phoning_info->date;
                else
                    $NowDate = date('d/m/Y');

                ?>
                <?php $data_input_date = array(
                        'name'        => 'date',
                        'id'          => 'date-fld',
                        'class'       => 'datepicker',
                        'value'       => @$NowDate,
                        'style'       => 'visibility: hidden',
                      );
            
                echo form_input($data_input_date); ?>
        </div>
    </div>
    <?php echo form_fieldset_close(); ?>

    <?php echo form_submit('submit', 'Valider', 'class="right button"'); ?>
<?php echo form_close(); ?>