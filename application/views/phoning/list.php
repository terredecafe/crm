
<table class="responsive">
    <thead>
        <th>Date</th>
        <th>Société</th>
        <th>Contact</th>
        <th>Tel</th>
        <th>GSM</th>
        <th>Note</th>
        <th>Cloturer</th>
    </thead>
    <tbody>
    <?php
    foreach ($infos_contact as $result):
    ?>
        <tr>
        <?php
            // Récupération informations contact
            $Contact = $result->n_prenom . ' ' . $result->n_nom;
            $Tel = $result->n_gsm;
            $Societe = $result->e_nom_societe;
        
            // Gestion du status
            // TODO : En fonction de la liste de Monelle !!!!!!!!!!!!
            $status = 'nc';
            if ($result->p_cloture)
                $status = 'Cloture';
            if ($result->p_objection)
                $status = 'Objection';

            if ($result->p_result_rdv)
                $date = date_us_to_fr($result->p_result_rdv_date);
            elseif($result->p_result_rappel)
                $date = date_us_to_fr($result->p_result_rappel_date);


            $link = site_url('phoning/?mode=update&id='.$result->p_id.'&id_contact='.$result->c_id);
            $link_fiche =site_url('fichier/?mode=menu_client&id_company='.$result->id_client);
            $link_fiche_contact =site_url('fichier/?mode=fiche_contact&action=update&id_client='.$result->id_client.'&id_contact='.$result->c_id);
            $ClotureLink = site_url('phoning/?cloture=true&id='.$result->p_id.'&id_contact='.$result->c_id);

            // CAS PARTICULIER : Terre de Café, Virginie
            /*
            $Who = array(
                '8' => 'Magaly',
                '9' => 'Charles',
                '22' => 'Virginie'
                );
            if ( $user->id_entite == '1' )
                $date .= ' ('.$Who[$result->p_id_user].')';

            */

        ?>
            <td><a href="<?php echo $link; ?>"><?php echo $date; ?></a></td>
            <td><a href="<?php echo $link_fiche; ?>"><?php echo $Societe; ?></a></td>
            <td><a href="<?php echo $link_fiche_contact; ?>"><?php echo $Contact; ?></a></td>
            <td><a href="tel:<?php echo $result->n_telephone; ?>"><?php echo $result->n_telephone; ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo $result->n_gsm; ?></a></td>
            <td><a href="<?php echo $link; ?>"><?php echo substr($result->p_commentaire, 0, 120); ?></a></td>
            <td><a href="<?php echo $ClotureLink; ?>"><span class="label">GO !</span></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>