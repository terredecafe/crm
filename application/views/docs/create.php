<?php
if ($this->input->get('mode') == 'update'):
  $hidden = array(
      'type'          => $this->input->get('mode'),
      'id_docs'       => $this->input->get('id'),
      'id_entreprise' => $this->input->get('id_entreprise'),
      'MAX_FILE_SIZE' => '9291456' //6mo
      );

elseif($this->input->get('mode') == 'create' AND $this->input->get('id_entreprise') != ''): // Si la création vient d'une fiche Client
  $hidden = array(
      'type'          => $this->input->get('mode'),
      'id_entreprise' => $this->input->get('id_entreprise'),
      'special'       => true,
      'MAX_FILE_SIZE' => '9291456'
      );
else:
  $hidden = array(
      'type'          => $this->input->get('mode'),
      'id_entreprise' => $this->input->get('id_entreprise'),
      'MAX_FILE_SIZE' => '9291456'
      );
endif;

$options_company = array();
foreach ($list_company->result() as $result):
  $options_company[$result->id] = $result->nom_societe;
endforeach;

?>

<?php echo form_open_multipart(current_url(), '', $hidden);?>
    <?php echo form_fieldset(''); ?>
    <div class="row">
        <div class="four columns">
        <?php if (isset($_GET['id_client'])): ?>
          <h2><?php echo $id_entreprise; ?></h2>
        <?php else: ?>
          <label for="company-fld">Soci&eacute;t&eacute; :</label>
          <?php 
          if (@$company_infos->nom_societe):
            echo $company_infos->nom_societe; 
          else:
            $company_checked = @$company_infos->nom_societe;
            echo form_dropdown('id_entreprise', $options_company, $company_checked, 'id="company-fld" class="ui-select"');
          endif; ?>
        <?php endif; ?>
        </div>
        <div class="four columns">
            <label for="type-fld">Type</label>
            <?php
            $options_type = $type_list;
            
            $type_checked = @$info_doc->type;                
            echo form_dropdown('type_doc', $options_type, $type_checked, 'class="ui-select" id="type-fld"'); ?>
        </div>
        <div class="four columns">
            <label for="date-fld">Date</label>
            <?php 
            if (@isset($phoning_info->date))
                    $NowDate = $phoning_info->date;
                else
                    $NowDate = date('d/m/Y');

            $data_input_date = array(
                    'name'        => 'date',
                    'id'          => 'date-fld',
                    'class'          => 'datepicker',
                    'value'       => $NowDate,
                  );
            
            echo form_input($data_input_date); ?>
        </div>
    </div>
    <?php echo form_fieldset_close(); ?>
    <?php echo form_fieldset(); ?>
    <div class="row">
        <div class="six columns">
            <label for="company-fld">Nom</label>
            <?php $data_nom = array(
            'name'        => 'name',
            'id'          => 'name-fld',
            'value'       => @$info_doc->name,
            );
            echo form_input($data_nom); ?>      
        </div>
        <div class="six columns">
            <label for="file-fld">Document</label>
            <input type="file" name="doc" id="file-fld"> (9mo max)
            <?php 
            if (@$info_doc->file):
            echo '<br /><br />';
            echo '<a target="_blank" href="'.site_url().'uploads/'.$info_doc->file.'"><span class="button success">'.$info_doc->name.'</span></a>'; 
            else:
            echo '<br /><br />';
            echo '<span class="button alert">Aucun document !</span>';
            endif;
            ?>
        </div>
    </div>
    <?php echo form_fieldset_close(); ?>
    <?php echo form_submit('submit', 'Valider', 'class="right button"'); ?>
<?php echo form_close(); ?>    