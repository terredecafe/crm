<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Simplelogin Class
 *
 * Makes authentication simple
 *
 * Simplelogin is released to the public domain
 * (use it however you want to)
 * 
 * Simplelogin expects this database setup
 * (if you are not using this setup you may
 * need to do some tweaking)
 * 

	#This is for a MySQL table
	CREATE TABLE `users` (
	`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`email` VARCHAR( 64 ) NOT NULL ,
	`password` VARCHAR( 64 ) NOT NULL ,
	UNIQUE (
	`email`
	)
	);

 * 
 */
class Simplelogin
{
	var $CI;
	var $user_table = 'users';
	var $logged_in = false;

	function __construct()
	{
		// get_instance does not work well in PHP 4
		// you end up with two instances
		// of the CI object and missing data
		// when you call get_instance in the constructor
		$this->CI =& get_instance();
		$this->CI->load->helper('cookie');
		
	    $this->check_login();
	}

    function is_logged_in()
    {
        return $this->logged_in;
    }
    
    function check_login()
    {
        $is_logged_in = get_cookie('ci_user_login');
        if ($is_logged_in && !$this->CI->session->userdata('user_logged_in'))
        {
            $this->CI->session->set_userdata(unserialize($is_logged_in));	
            $this->CI->session->set_userdata(array('user_logged_in' => true));
        }
        
        $this->logged_in = $this->CI->session->userdata('user_logged_in');
    }
    
	/**
	 * Login and sets session variables
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function login($user = '', $password = '', $autologin = false) {	
        
		//Make sure login info was sent
		if($user == '' OR $password == '') {
			return false;
		}

		//Check if already logged in
		if($this->CI->session->userdata('user_logged_in') == $user) {
			//User is already logged in.
			return false;
		}
		
		//Check against user table
		$this->CI->db->where('prenom', $user); 
		echo $user;
		$query = $this->CI->db->get_where($this->user_table);
		
		if ($query->num_rows() > 0) {

			$row = $query->row_array(); 
			
			//Check against password;
			if(md5($password) != $row['password']) {
				return false;
			}
			
			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();
			
			
			
			//Remove the password field
			unset($row['password']);
			
			//Set session data
			$this->CI->session->set_userdata($row);	
			
			if ($autologin)
			{
                $cookie = array(
                    'name'   => 'ci_user_login',
                    'value'  => serialize($row),
                    'expire' => '86500',
                );
                
                set_cookie($cookie);
			}
			
			$this->logged_in = true;		
			
			//Login was successful			
			return true;
		} else {
			//No database result found
			return false;
		}	

	}

	/**
	 * Logout user
	 *
	 * @access	public
	 * @return	void
	 */
	function logout() {
		//Put here for PHP 4 users
		$this->CI =& get_instance();	
		
		delete_cookie("ci_user_login");	

		//Destroy session
		$this->CI->session->sess_destroy();
	}
}