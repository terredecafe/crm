<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class csv_reader {
    
    var $fields;            /** columns names retrieved after parsing */
    var $separator = ';';    /** separator used to explode each line */
    
 
    function parseText($p_Text) {
        $lines = explode("\n", $p_Text);
        return $this->parseLines($lines);
    }
    

    function parseFile($p_Filepath) {
        $lines = file($p_Filepath);
        return $this->parseLines($lines);
    }
    
 
    function parseLines($p_CSVLines) {    
        $content = FALSE;
        $LOL = array();
        $i=0;
        foreach( $p_CSVLines as $line_num => $line ) {
            if( $line != '' ) { // skip empty lines
                $elements = @split($this->separator, $line);

                $LOL[$i] = $elements[0].'-'.$elements[1];
                
            }
        $i++;
        }
        return $LOL;
    }
}