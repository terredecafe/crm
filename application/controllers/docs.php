<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docs extends CI_Controller {

    function __construct()
    {   
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('phoning_model');
        $this->load->model('taches_model');
       	$this->load->model('docs_model'); 
        $this->load->model('user_model');
        $this->load->model('fichier_model');
        
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');



        if (!user_logged_in() && current_url() != site_url('user/login'))
        {
            redirect(site_url('user/login'));
        }
    }

	public function index()
	{

	    if ($this->input->post('type')):

	    	switch ($this->input->post('type')):
	    		case 'add': $this->record(); break;
	    		case 'update': $this->update(); break;
	    		case 'create': $this->record(); break;
	    	endswitch;
	    elseif($this->input->post('filters')):

	    	$this->view();
	    else:
	    	$this->view();
	    endif;
	}
	
	public function view()
	{

	    $datas = array();
	    $ByClient = false;

	    if ($this->input->get('id_entreprise')):
	    	$ByClient = true;
	    	$id_entreprise = $this->input->get('id_entreprise');
	    	$tmp = $this->fichier_model->get_company_info($id_entreprise);
	    	$nom_societe = $tmp->nom_societe;
	    else:
	    	$ByClient = false;
	   endif;

	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = $datas['user']->prenom . ' ' . $datas['user']->nom; 
	    $Fiche_client_link = site_url('fichier/?mode=menu_client&id_company='.$this->input->get('id_entreprise'));

	    if ($ByClient)  
	    	$datas['header_msg'] = '<i class="gen-enclosed icon-folder"></i> Documents de <a href="'.$Fiche_client_link.'">'. $nom_societe.'</a>';
	    else
	    	$datas['header_msg'] = '<i class="gen-enclosed icon-folder"></i> Mes Documents';	

		
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;
 
	    // Récupération de la liste des types
	    $datas['type_list'] = $this->docs_model->get_type_list($datas['user']->id_entite);

	    if ($this->input->post('filters')):

	    	if ($ByClient) // Si on filtre par Clients ! ($ByClient true)
		    	$datas['results'] = $this->docs_model->get_list_w_filter_by_client($id_user, $this->input->post('type_filter'), $id_entreprise);
		    else
		    	$datas['results'] = $this->docs_model->get_list_w_filter($id_user, $this->input->post('type_filter'));

		    	$datas['selected_filter'] = $this->input->post('type_filter');
	    else:
	    	if ($ByClient) // Si on filtre par Clients ! ($id_client true)
		    	$datas['results'] = $this->docs_model->get_list_by_client($id_user, $id_entreprise);
		    else
		    	$datas['results'] = $this->docs_model->get_list($id_user);
		    	$datas['selected_filter'] = 'all';
	    endif;

	    
	    switch ($this->input->get('mode')):

	    	default: // LIST

		    	$this->load->view('header', $datas);
		    	$this->load->view('docs/list', $datas);
		    	$this->load->view('footer');
	    	break;

	    	case 'create': // CREATION
	    	
	    		if ($id_user = '22'):
	    			$datas['list_company'] = $this->taches_model->get_list_company_virginie();
	    		else:
	    			$datas['list_company'] = $this->taches_model->get_list_company($id_user);
	    		endif;


	    		$datas['id_entreprise'] = $this->input->get('id_entreprise');


	    		$datas['company_infos'] = $this->phoning_model->get_company_info($datas['id_entreprise']);

	    		$datas['header_msg'] = 'Ajout d\'un document pour '.@$datas['company_infos']->nom_societe;

	    		/* Chargement des vues */
		    	$this->load->view('header', $datas);
		    	$this->load->view('docs/create', $datas);
		    	$this->load->view('footer');	    		
	    	break;

	    	case 'update': // MODIFICATION   		

	    		$id_doc = $this->input->get('id');
	    		$datas['info_doc'] = $this->docs_model->get_doc($id_doc);
   	
	    		// Récupération des informations de la ligne de phoning concernée
	    		$datas['list_company'] = $this->taches_model->get_list_company($id_user);
	    		$datas['id_company'] = $this->input->get('id_entreprise');
	    		$datas['company_infos'] = $this->phoning_model->get_company_info($datas['id_company']);

	    		$datas['header_msg'] = 'Modification du document : '.strtoupper($datas['info_doc']->name);

		    	$this->load->view('header', $datas); 
		    	$this->load->view('docs/create', $datas);
		    	$this->load->view('footer');	    		
	    	break;

	    	case 'delete': // SUPPRESSION
	    		
	    		// Récupération des informations de la ligne de phoning concernée
	    		$datas['list_company'] = $this->taches_model->get_list_company($id_user);
	    		$datas['id_company'] = $this->input->get('id_entreprise');
	    		$datas['company_infos'] = $this->phoning_model->get_company_info($datas['id_company']);

	    		$datas['header_msg'] = 'Ajout d\'un document pour '.@$datas['company_infos']->nom_societe;
	    		$Delete = $this->docs_model->delete_doc($this->input->get('id'));

	    		// Suppression du fichier dans ./uploads
	    		$Doc = $this->docs_model->get_doc($this->input->get('id'));
	    		$Unlink = unlink('./uploads/'.$Doc->file);

	    		// Redirection
	    		$geturl  = 'docs/?id_entreprise='.$this->input->get('id_entreprise');
				redirect(site_url().$geturl);


	    	break;

	    endswitch;
	}

	public function record()
	{	



		$id_user = $this->session->userdata('id');

		$dossier = getcwd() . '/uploads/';
		$fichier = basename($_FILES['doc']['name']);
		$taille_maxi = $this->input->post('MAX_FILE_SIZE');
		$taille = filesize($_FILES['doc']['tmp_name']);


		if($taille > $taille_maxi)
		{
		    $error = 'Le fichier est trop gros...';
		}
		if(!isset($error)): //S'il n'y a pas d'erreur, on upload

		    $fichier = strtr($fichier, 
		          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
		          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		    $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

		    $fichier = str_replace(' ', '_', $fichier);

		    if(move_uploaded_file($_FILES['doc']['tmp_name'], $dossier . $fichier))
				$upload = true;
		    else 
		        $upload = false;
		else:
			$upload = false;
		endif;

		$DatasInsert = array(
		   'id_user'			=> $id_user,
		   'id_entreprise' 		=> $this->input->post('id_entreprise'),
		   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
		   'name' 				=> $this->input->post('name'),
		   'file'				=> $fichier,
		   'type'				=> $this->input->post('type_doc')
		);

		$this->db->insert('docs', $DatasInsert);

		$last_id = $this->db->insert_id();

		if (!isset($error)): // SI 0 ERREUR, ON RECORD EN DB		

			if ($this->input->post('special')): // SI la création est lancée depuis une fiche client, pour y retourner

				$geturl  = 'docs/?id_entreprise='.$this->input->post('id_entreprise');
				// Redirection 
				redirect(site_url().$geturl);

			endif;

			$this->view();

		else:
			
			$geturl  = 'docs/?mode=update&id='.$last_id.'&id_entreprise='.$this->input->post('id_entreprise');
			// Redirection 
			redirect(site_url().$geturl);

		endif;

		

	}

	public function update()
	{
		$id_user = $this->session->userdata('id');
		$dossier = getcwd() . '/uploads/';
		$fichier = basename($_FILES['doc']['name']);
		$taille_maxi = $this->input->post('MAX_FILE_SIZE');
		$taille = filesize($_FILES['doc']['tmp_name']);

		if (isset($_FILES['doc']['name'])):
			if($taille>$taille_maxi)
			{
			    $error = 'Le fichier est trop gros...';
			}
			if(!isset($error)): //S'il n'y a pas d'erreur, on upload

			    $fichier = strtr($fichier, 
			          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
			          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
			    $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

			    $fichier = str_replace(' ', '_', $fichier);

			    if(move_uploaded_file($_FILES['doc']['tmp_name'], $dossier . $fichier))
					$upload = true;
			    else 
			        $upload = false;
			else:
				$upload = false;
			endif;
		endif;

		if ($fichier):
			$DatasUpdate = array(
			   'id_user'			=> $id_user,
			   'id_entreprise' 		=> $this->input->post('id_entreprise'),
			   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
			   'name' 				=> $this->input->post('name'),
			   'file'				=> $fichier,
			   'type'				=> $this->input->post('type_doc')
			);
		else:
			$DatasUpdate = array(
				   'id_user'			=> $id_user,
				   'id_entreprise' 		=> $this->input->post('id_entreprise'),
				   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
				   'name' 				=> $this->input->post('name'),
				   'type'				=> $this->input->post('type_doc')
				);
		endif;

		$this->db->where('id', $this->input->post('id_docs'));
		$this->db->update('docs', $DatasUpdate); 

		if (!isset($error)): // SI 0 ERREUR, ON RECORD EN DB		

			if ($this->input->post('special')): // SI la création est lancée depuis une fiche client, pour y retourner

				$geturl  = 'fichier/?mode=docs&id_client='.$this->input->post('id_entreprise');
				// Redirection 
				redirect(site_url().$geturl);

			endif;



			$this->view();

		else:
			
			$geturl  = 'docs/?mode=update&id='.$last_id.'&id_company='.$this->input->post('id_entreprise').'&error=1';
			// Redirection 
			redirect(site_url().$geturl);

		endif;

	}	    	

}

/* End of file docs.php */
/* Location: ./application/controllers/welcome.php */