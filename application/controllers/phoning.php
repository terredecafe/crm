<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phoning extends CI_Controller {

    function __construct()
    {   
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->model('phoning_model');
        $this->load->model('user_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');
        
        if (!user_logged_in() && current_url() != site_url('user/login'))
        {
            redirect(site_url('user/login'));
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    if ($this->input->post()):

	    	switch ($this->input->post('type')):
	    		case 'add': $this->record(); break;
	    		case 'update': $this->update(); break;
	    		case 'create': $this->record(); break;
	    	endswitch;
	    elseif(@$this->input->get('cloture')):
	    	$this->cloture();
	    else:
	    	$this->view();
	    endif;
	}
	
	public function view()
	{
	    $datas = array();
	    
	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-phone"></i> Mon phoning';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;

	    $datas['id_client'] = $this->input->get('id_client');
	    $datas['id_contact'] = $this->input->get('id_contact'); // TEMPORAIRE ------------------------



	    $datas['results'] = $this->phoning_model->get_list($id_user);
	    $datas['infos_contact'] = $this->phoning_model->get_contact_infos($datas['id_contact']);
	    $datas['infos_client'] = $this->phoning_model->get_company_info($datas['id_client']);

	    
	    
	    switch ($this->input->get('mode')):

	    	default: // LIST

	    		$temp = $this->phoning_model->get_list($id_user);
	    		$datas['infos_contact'] = $temp->result();

		    	$this->load->view('header', $datas);
		    	$this->load->view('phoning/list', $datas);
		    	$this->load->view('footer');
	    	break;

	    	case 'create': // CREATION
	    		$datas['contact'] = $this->phoning_model->get_list_contact_from_client($datas['id_client']);
	    		$Contact = @$datas['infos_contact']->prenom . ' ' . @$datas['infos_contact']->nom;
	    		$datas['header_msg'] = 'Cr&eacute;ation d\'un appel concernant : ';
	    		$datas['header_msg'] .= $Contact;
	    		$datas['id_entreprise'] = $datas['infos_contact']->id;

	    		/* Chargement des vues */
		    	$this->load->view('header', $datas);
		    	$this->load->view('phoning/create', $datas);
		    	$this->load->view('footer');	    		
	    	break;

	    	case 'update': // MODIFICATION   		
	    		$datas['infos_contact'] = $this->phoning_model->get_contact_infos($this->input->get('id_contact'));

	    		$Contact = @$datas['infos_contact']->prenom . ' ' . @$datas['infos_contact']->nom;
	    		$datas['header_msg'] = 'Modification d\'un appel : ';
	    		$datas['header_msg'] .= $Contact;

	    		// Récupération des informations de la ligne de phoning concernée
	    		$datas['id_phoning'] = $this->input->get('id');
	    		$datas['phoning_info'] = $this->phoning_model->get_phoning_info($this->input->get('id'));


	    		if(@$datas['phoning_info']->objection)
	    			@$datas['phoning_info']->objection = TRUE;
	    		else 
	    			@$datas['phoning_info']->objection = FALSE;

	    		if(@$datas['phoning_info']->email)
	    			@$datas['phoning_info']->email = TRUE;
	    		else 
	    			@$datas['phoning_info']->email = FALSE;	

	    		@$datas['phoning_info']->date =  @date_us_to_fr(@$datas['phoning_info']->date);

	    		if (@$datas['phoning_info']->result_rdv):
	    			$datas['phoning_info']->resultat = 'rdv';
	    			$datas['phoning_info']->resultat_date = date_us_to_fr($datas['phoning_info']->result_rdv_date);
	    			$datas['phoning_info']->time_rdv_rappel = get_heure_from_us_date($datas['phoning_info']->result_rdv_date);
	    		elseif(@$datas['phoning_info']->result_rappel):
	    			$datas['phoning_info']->resultat = 'rappel';
	    			$datas['phoning_info']->resultat_date = date_us_to_fr($datas['phoning_info']->result_rappel_date);
	    			$datas['phoning_info']->time_rdv_rappel = get_heure_from_us_date($datas['phoning_info']->result_rappel_date);
	    		endif;

		    	$this->load->view('header', $datas);
		    	$this->load->view('phoning/create', $datas);
		    	$this->load->view('footer');	    		
	    	break;

	    endswitch;
	}

	public function record()
	{
		switch($this->input->post('resultat')):
			case 'rdv':
				$result_rdv = 1;
				$result_rdv_date = date_fr_to_us($this->input->post('date_rdv_rappel'), $this->input->post('time_rdv_rappel'));
				$result_duree_rdv = $this->input->post('result_duree_rdv');

				$result_rappel = 0;				
				$result_rappel_date = 0;
				
				$infocontact 	= 	$this->phoning_model->get_contact_infos($this->input->post('id_contact'));
			    $boite			= 	$this->phoning_model->get_company_info($this->input->post('id_entreprise'));
			    echo $this->db->last_query();

			    $datas['user'] = $this->user_model->get_user_datas($this->session->userdata('id'));




				// INJECTION GOOGLE CALENDAR
				$summary = $boite->nom_societe . ' à '.$boite->ville;
				$description = '';
				$location = '';
				$start_date = date_fr_to_us($this->input->post('date_rdv_rappel'));
				$start_time = $this->input->post('time_rdv_rappel');
				$end_date 	= date_fr_to_us($this->input->post('date_rdv_rappel'));
				$email 		= $datas['user']->email;
				//$email = 'jonathan.guery@terredecafe.fr';			

			break;
			case 'rappel':
				$result_rappel = 1;
				$result_rappel_date = date_fr_to_us($this->input->post('date_rdv_rappel'), $this->input->post('time_rdv_rappel'));

				$result_rdv = 0;
				$result_rdv_date = 0;
				$result_duree_rdv = 0;
			break;

		endswitch;


	
		$id_user = $this->session->userdata('id');

		$DatasInsert = array(
		   'id_contact' 		=> $this->input->post('id_contact'),
		   'id_user'			=> $id_user,
		   'id_entreprise'		=> $this->input->post('id_entreprise'),
		   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
		   'result_rdv' 		=> $result_rdv,
		   'result_rdv_date' 	=> $result_rdv_date,
		   'result_duree_rdv' 	=> $result_duree_rdv,
		   'result_rappel' 		=> $result_rappel,
		   'result_rappel_date' => $result_rappel_date,
		   'objection' 			=> $this->input->post('objection'),
		   'commentaire' 		=> $this->input->post('commentaires'),
		   'cloture' 			=> $this->input->post('cloture')
		);


 	

		if ($this->input->post('cloture')):
			redirect(site_url().'phoning/?mode=create&id='.$this->input->post('id_client').'&id_contact='.$this->input->post('id_contact'));
		else:
			$this->db->insert('phoning', $DatasInsert);
			$this->view();
			//redirect(site_url().'phoning/';
			if ($this->input->post('resultat') == 'rdv'):
				$link = $this->phoning_model->create_google_event($summary, $description, $location, $start_date, $start_time, $end_date, $email);				
			endif;			
		endif;				
	}

	public function update()
	{

		switch($this->input->post('resultat')):
			case 'rdv':
				$result_rdv = 1;
				$result_rdv_date = date_fr_to_us($this->input->post('date_rdv_rappel'), $this->input->post('time_rdv_rappel'));
				$result_duree_rdv = $this->input->post('result_duree_rdv');
				$result_rappel = 0;				
				$result_rappel_date = 0;
			break;
			case 'rappel':
				$result_rappel = 1;
				$result_rappel_date = date_fr_to_us($this->input->post('date_rdv_rappel'), $this->input->post('time_rdv_rappel'));

				$result_rdv = 0;
				$result_rdv_date = 0;
				$result_duree_rdv = 0;
			break;

		endswitch;


	

		$DatasUpdate = array(
		   'id_contact' 		=> $this->input->post('id_contact'),
		   'id_entreprise'		=> $this->input->post('id_entreprise'),
		   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
		   'result_rdv' 		=> $result_rdv,
		   'result_rdv_date' 	=> $result_rdv_date,
		   'result_duree_rdv' 	=> $result_duree_rdv,
		   'result_rappel' 		=> $result_rappel,
		   'result_rappel_date' => $result_rappel_date,
		   'objection' 			=> $this->input->post('objection'),
		   'commentaire' 		=> $this->input->post('commentaires'),
		   'cloture' 			=> $this->input->post('cloture'),
		   'email' 				=> $this->input->post('email')
		);

					

		//if ($this->input->post('cloture')):
			//$this->db->delete('phoning', array('id' => $this->input->post('id_phoning')));
			//redirect(site_url().'phoning/?mode=create&id_contact='.$this->input->post('id_contact'));			 
		//else:
			$this->db->where('id', $this->input->post('id_phoning'));
			$this->db->update('phoning', $DatasUpdate); 

			redirect(site_url().'phoning/?mode=update&id='.$this->input->post('id_phoning').'&id_contact='.$this->input->post('id_contact'));
		//endif;
	
	}	

	public function cloture()
	{
		$DatasUpdate = array(
		   'cloture' 			=> '1',
		);

		$this->db->where('id', $this->input->get('id'));
		$this->db->update('phoning', $DatasUpdate); 

		//$this->db->delete('phoning', array('id' => $this->input->get('id')));
		redirect(site_url().'phoning/?mode=create&id_contact='.$this->input->get('id_contact'));			
	}    	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */