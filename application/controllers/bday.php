<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bday extends CI_Controller {

    function __construct()
    {   
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('email');

        $this->load->model('user_model');
        $this->load->model('fichier_model');
        $this->load->model('taches_model');
        $this->load->model('phoning_model');
        $this->load->model('docs_model');
        
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');


    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$bdays = $this->fichier_model->BirthdaySearchAll();

		foreach($bdays->result() as $value):

			$Company = $this->fichier_model->get_company_info($value->id_entreprise); 

		
			$User = $this->user_model->get_user_datas($Company->id_user);

			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('crm-gf1@terredecafe.fr', 'CRM / Groupe Force 1');
			$this->email->to($User->email);
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');

			$SubjDest = $value->prenom . ' ' . $value->nom . ' (' .$Company->nom_societe.')';

			$this->email->subject('[CRM/GF1] Alerte Anniversaire : ' . $SubjDest);
			$this->email->message("Bonjour $User->prenom ! <br /><br /> <strong>Rappel :</strong><br /> $SubjDest fête son anniversaire aujourd'hui !");

			$this->email->send();
			echo $this->email->print_debugger();

		endforeach; 
	}
	

}

/* End of file fichier.php */
/* Location: ./application/controllers/fichier.php */