<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct()
    {   
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('user_model');
        $this->load->model('fichier_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');
    
        if (!user_logged_in() && current_url() != site_url('user/login') && current_url() != site_url('user/proceed'))
        {
            redirect(site_url('user/login'));
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    $this->view();
	}
	
	public function view()
	{
	    $datas = array();
	    $datas['user'] = $this->user_model->get_user_datas($this->session->userdata('id') );

	    //$datas['welcome'] = 'Bonjour ' . $datas['user']->prenom . ' ' . $datas['user']->nom;
	    $datas['welcome'] = $datas['user']->prenom . ' ' . $datas['user']->nom;
	    $datas['header_msg'] = 'Bienvenue '. $datas['user']->prenom . ' ' . $datas['user']->nom;

    	$datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;

		// Birthday search !
		$IsBirthdayTEMP = $this->fichier_model->BirthdaySearchByUser($datas['user']->id);
		$datas['IsBirthday'] = $IsBirthdayTEMP->result();

		$i = 0;
		foreach($datas['IsBirthday'] as $value):

			$datas['bday'][$i]['nom'] = $value->nom;
			$datas['bday'][$i]['prenom'] = $value->prenom;
			$datas['bday'][$i]['id_client'] = $value->id_entreprise;
			$datas['bday'][$i]['id_contact'] = $value->id;
			$datas['bday'][$i]['nom_societe'] = $value->nom_societe;

			$i++;
		endforeach;


	    $this->load->view('header', $datas);
	    $this->load->view('user/single', $datas);
	    $this->load->view('footer');
	}
	
	public function login()
	{

	    $datas['errors'] = $this->session->flashdata('errors');
	    
	    if (user_logged_in())
	    {
	        redirect(site_url('user'));
	    }

	    $datas['email'] = $this->session->userdata('email'); 
	    
	    $this->load->view('header', $datas);
	    $this->load->view('user/login', $datas);
	    $this->load->view('footer');
	}
	
	
	public function logout()
	{
	    $this->simplelogin->logout();
	    $this->session->sess_destroy();
	    redirect(site_url('user/login'));
	}
	
	public function proceed()
	{
	    $redirect = $this->input->post('url');
	    //$this->form_validation->set_rules('email','Email','required|valid_email');
	    $this->form_validation->set_rules('password','Mot de passe','required');
	    $this->session->set_userdata('email',$this->input->post('email'));
	    
	    if($this->form_validation->run())
	    {
	        if($this->simplelogin->login($this->input->post('email'), $this->input->post('password'), 1)) {
	            redirect($redirect);
	        }
	        else {
	            $this->session->set_flashdata('errors','<p>Invalid Password</p>');
	            redirect($redirect);
	        }
	    }
	    else
	    {
	        $this->session->set_flashdata('errors',validation_errors());
	        redirect($redirect);
	    }
	}
	
	
	private function add_string($str = '')
	{
	    return $str.'&euro;';
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */