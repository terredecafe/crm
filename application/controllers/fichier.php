<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fichier extends CI_Controller {

    function __construct()
    {   
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('email');

        $this->load->model('user_model');
        $this->load->model('fichier_model');
        $this->load->model('taches_model');
        $this->load->model('phoning_model');
        $this->load->model('docs_model');
        $this->load->model('export_model');
        
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');

        if ($this->input->get('mode') != 'bday_check'):
	        if (!user_logged_in() && current_url() != site_url('user/login'))
	            redirect(site_url('user/login'));
	    endif;
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

	    if (@$this->input->post('action')):

	    	if ($this->input->post('page') == 'fiche_client'):
	    	
		    	switch ($this->input->post('action')):
		    		case 'update': $this->update_fichier(); break;
		    		case 'create': $this->record_fichier(); break;
		    	endswitch;

		    endif;

		    if($this->input->post('page') == 'fiche_contact'):

		    	switch ($this->input->post('action')):
		    		case 'update': $this->update_contact(); break;
		    		case 'create': $this->record_contact(); break;
		    	endswitch;

		    endif;

	    else:
	    	$this->view();
	    endif;
	}
	
	public function view()
	{
	    $datas = array();

	    
	    $id_user = $this->session->userdata('id');

	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> Mon Fichier';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);


	    $datas['logo_entite'] = $datas['entite']->logo;

	    switch ($this->input->get('mode')):

	    	default: // Search
	    		
	    		$tmp = $this->fichier_model->get_search_list($id_user);
	    		$tmpContact = $this->fichier_model->get_search_list_contact($id_user);
	    		$tmpcount = $this->fichier_model->get_list_count($id_user);

	    		$ResultContact = $tmpContact->result();
	    		$datas['contact_list'] = array(); 

	    		foreach($ResultContact as $values):
					$datas['contact_list'][$values->id_entreprise] = $values->nom . ' ' .$values->prenom;
	    		endforeach;

	    		$datas['usercount'] = $tmpcount->result();
	    		$datas['userlist'] = $tmp->result();
	    		$datas['query'] = $this->fichier_model->get_query_export($id_user);

		    	$this->load->view('header', $datas);
		    	$this->load->view('fichier/search', $datas);
		    	$this->load->view('footer');

	    	break;

	    	case 'delete':

	    		// Modification du status PUBLISHED -> TRASH + QUI QUAND !
				$mysqldate = date( 'Y-m-d H:i:s' );	    	

		    	$DatasUpdate = array(
			   'status'			=> 'TRASH',
			   'deleted_when'	=> $mysqldate,
			   'deleted_who' 	=> $id_user
				);

				$this->db->where('id', $this->input->get('id_company'));
				$this->db->update('entreprises', $DatasUpdate); 


	    		// Suppression
	    		//$temp = $this->fichier_model->delete_fiche($this->input->get('id_company'));

   				redirect(site_url().'fichier/');

	    	break;

	    	case 'bday_check':

	    		$bdays = $this->fichier_model->BirthdaySearchAll();

				foreach($bdays->result() as $value):

					$Company = $this->fichier_model->get_company_info($value->id_entreprise); 

				
					$User = $this->user_model->get_user_datas($Company->id_user);

					$config['mailtype'] = 'html';
					$this->email->initialize($config);

					$this->email->from('crm-gf1@terredecafe.fr', 'CRM / Groupe Force 1');
					$this->email->to($User->email);
					//$this->email->cc('another@another-example.com');
					//$this->email->bcc('them@their-example.com');

					$SubjDest = $value->prenom . ' ' . $value->nom . ' (' .$Company->nom_societe.')';

					$this->email->subject('CRM / Alerte Anniversaire : ' . $SubjDest);
					$this->email->message("Bonjour $User->prenom ! <br /><br /> <strong>Rappel :</strong><br /> $SubjDest fête son anniversaire aujourd'hui !");

					$this->email->send();


				endforeach; 

	    		$this->load->view('header', $datas);
		    	$this->load->view('fichier/bday_check', $datas);
		    	$this->load->view('footer');
	    	break;

	    	case 'menu_client': // Le menu client avec les différents accès

	    		
	    		if($this->input->post('entreprise')):
	    			$datas['id_client'] = $this->input->post('entreprise');
	    		elseif($this->input->get('id_company')):
	    			$datas['id_client'] = $this->input->get('id_company');	
	    		endif;
	    		
	    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
	    		
	    		$temps = $this->fichier_model->get_first_contact_id($datas['id_client']);		    		
	    		$datas['contact_principal'] = $temps->result();

	    		if(@$datas['contact_principal'][0]->id):
		    		$datas['id_contact'] = @$datas['contact_principal'][0]->id;	    	
		    		$datas['no_first_contact'] = false;	
   				else:
   					$datas['no_first_contact'] = true;
   				endif;



	    		$datas['nom_client'] = $temp->nom_societe;
	    		$datas['ville'] = $temp->ville;
	    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> '.$datas['nom_client'].' - <span> '.$datas['ville'].' </span>';

		    	$this->load->view('header', $datas);
		    	$this->load->view('fichier/menu_client', $datas);
		    	$this->load->view('footer');	
		    	    		
	    	break;
	    	
	    	case 'fiche_client': // Ajout & Modification de la fiche client


	    		// Liste des familles
	    		$datas['list_famille'] = $this->fichier_model->get_list_famille_by_entite($datas['user']->id_entite);

	    		// Liste des commerciaux
	    		$datas['list_commercial'] = $this->fichier_model->get_list_commercial();

	    		// Liste des effectifs
	    		$datas['list_effectif'] = $this->fichier_model->get_list_effectif();

				// Liste des effectifs
	    		$datas['list_qualif'] = $this->fichier_model->get_list_qualif();

	    		// Liste des effectifs
	    		$datas['list_zones_geo'] = $this->fichier_model->get_list_zones_geo_by_entite($datas['user']->id_entite);

	    		// Liste des entité
	    		$datas['list_entite'] = $this->fichier_model->get_list_entite();

	    		if (@$this->input->get('action') == 'create'): // AJOUT
		    		$datas['id_client'] = $this->input->get('id_client');
		    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
		    		$datas['client']->id_entite  = $datas['user']->id_entite;
		    		$datas['client']->id_user = $datas['user']->id;

		    		if ($id_user == $datas['client']->id_user OR $îd_user == 2) // ou si c'est Malik
		    			$datas['IsAdmin'] = true;
		    		else
		    			$datas['IsAdmin'] = false;

	    			$this->load->view('header', $datas);
			    	$this->load->view('fichier/fiche_client', $datas);
			    	$this->load->view('footer');	

	    		else: // MODIFICATION
		    		$datas['id_client'] = $this->input->get('id_client');
		    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
		    		$datas['client'] = $temp;
		    		$datas['nom_client'] = $temp->nom_societe;
		    		$Fiche_client_link = site_url('fichier/?mode=menu_client&id_company='.$datas['id_client']);
		    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> Fiche client <span> <a href="'.$Fiche_client_link.'">'.$datas['nom_client'] . '</a> ';


		    		// Récupération du contact principal
		    		$temp = $this->fichier_model->get_first_contact_id($this->input->get('id_client'));		    		
		    		$datas['contact_principal'] = $temp->result();
		    		$datas['contact_principal'] = @$datas['contact_principal'][0];

		    		// Test si la fiche appartient au commercial identifié
		    		if ($id_user == $datas['client']->id_user OR $id_user == 2) // ou si c'est Malik
		    			$datas['IsAdmin'] = true;
		    		else
		    			$datas['IsAdmin'] = false;

		    		// Test si le nom existe déjà après la création d'une fiche (param get $check)
		    		if ($this->input->get('check')):

		    			$Temp = $this->export_model->simple_search_before_doublon($datas['client']->nom_societe, $this->input->get('id_client'));
		    			$IsExist = $Temp->result();

		    			if ($IsExist):
		    				$datas['IsExist'] = true;
		    				$Who = $this->export_model->simple_search_doublon($datas['client']->nom_societe);
		    				$datas['WhoIs'] = $Who->result();

		    			else:
		    				$datas['IsExist'] = false;
		    			endif;

		    		endif;

			    	$this->load->view('header', $datas);
			    	$this->load->view('fichier/fiche_client', $datas);
			    	$this->load->view('footer');	
		    	endif;

	    	break;

	    	case 'list_contacts': 

	    		// Liste des fonctions pour un Contact
	    		$datas['list_fonctions'] = $this->fichier_model->get_list_fonctions_contact_by_entite($datas['user']->id_entite);

	    		$datas['id_client'] = $this->input->get('id_client');
	    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
	    		$datas['client'] = $temp;
	    		$datas['nom_client'] = $temp->nom_societe;

	    		$Fiche_client_link = site_url('fichier/?mode=menu_client&id_company='.$datas['id_client']);

	    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> <a href="'.$Fiche_client_link.'">'.$datas['nom_client'] . '</a> Liste des contacts';

	    		// Récupération de la liste des contacts
	    		$datas['list_contacts'] = $this->fichier_model->get_list_contacts($datas['id_client']);	 

	    		if (@$this->input->get('action') == 'delete'):


	    			// DELETE
	    			$this->db->delete('contacts', array('id' => $this->input->get('id_contact')));

		    		$datas['id_client'] = $this->input->get('id_client');
		    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
		    		$datas['client'] = $temp;
		    		$datas['nom_client'] = $temp->nom_societe;
		    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> Fiche contact ('.$datas['nom_client'].')';
		    		// Récupération de la liste des contacts
		    		$datas['list_contacts'] = $this->fichier_model->get_list_contacts($datas['id_client']);	 	

			    	$this->load->view('header', $datas);
			    	$this->load->view('fichier/list_contacts', $datas);
			    	$this->load->view('footer');		  
			    else:

		    	$this->load->view('header', $datas);
		    	$this->load->view('fichier/list_contacts', $datas);
		    	$this->load->view('footer');

		    	endif;


		    break;

	    	case 'fiche_contact':
	    		$datas['id_client'] = $this->input->get('id_client');
	    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
	    		$datas['client'] = $temp;
	    		$datas['nom_client'] = $temp->nom_societe;
	    		$Fiche_client_link = site_url('fichier/?mode=menu_client&id_company='.$datas['id_client']);
	    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i>  ( <a href="'.$Fiche_client_link.'">'.$datas['nom_client'].'</a>)';


	    		
	    		// Récupération des informations du contact principal
	    		$id_contact = $this->input->get('id_contact');
	    		$datas['contact_info'] = $this->fichier_model->get_contact_info($id_contact);	

	    		// Liste des fonctions pour un Contact
	    		$datas['list_fonctions'] = $this->fichier_model->get_list_fonctions_contact_by_entite($datas['user']->id_entite);

	    		if (@$this->input->get('first'))
	    			$datas['first_contact'] = true;

	    		if (@$this->input->get('action') == 'create'):
	    			
	    			$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> '.$datas['nom_client'].' <span>Création d\'un contact pour le client</span>';
	    			//print_r($datas);
	    			$datas['contact_info'] =  $this->fichier_model->get_company_info($datas['client']->id);
			    	$this->load->view('header', $datas);
			    	$this->load->view('fichier/fiche_contact', $datas);
			    	$this->load->view('footer');
  	

	    		else:

	    			$datas['header_msg'] = $datas['contact_info']->prenom.' '.$datas['contact_info']->nom.' ('.$datas['nom_client'].')';

	    			// Calcul date anniversaire
	    			if($datas['contact_info']->birthday == date('d/m'))
	    				$datas['header_msg'] .= '<strong style="color:red;"> C\'est son anniversaire !!</strong>';

			    	$this->load->view('header', $datas);
			    	$this->load->view('fichier/fiche_contact', $datas);
			    	$this->load->view('footer');

	    		endif;

		    break;
		    
		    case 'docs':

	    		$datas['id_client'] = $this->input->get('id_entreprise');
	    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
	    		$datas['client'] = $temp;
	    		$datas['nom_client'] = $temp->nom_societe;
	    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> Liste des documents pour ' . $datas['nom_client'];
			    // Récupération de la liste des types
			    $datas['type_list'] = $this->docs_model->get_type_list();

	    		// Récupération de la liste des documents
	    		$datas['docs'] = $this->docs_model->get_doc_by_client($id_user, $datas['id_client']);	 	

		    	$this->load->view('header', $datas);
		    	$this->load->view('fichier/documents', $datas);
		    	$this->load->view('footer');

		    break;

		    case 'historique_rappel':

	    		$datas['id_client'] = $this->input->get('id_client');
	    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
	    		$datas['client'] = $temp;
	    		$datas['nom_client'] = $temp->nom_societe;

	    		$temps = $this->fichier_model->get_first_contact_id($datas['id_client']);		    		
	    		$datas['contact_principal'] = $temps->result();

	    		if(@$datas['contact_principal'][0]->id):
		    		$datas['id_contact'] = @$datas['contact_principal'][0]->id;	    	
		    		$datas['no_first_contact'] = false;	
   				else:
   					$datas['no_first_contact'] = true;
   				endif;	    		

	    		// count
	    		$temps =  $this->phoning_model->get_count_tache($id_user, $temp->id, 'rappel');
	    		$count = $temps->result();
	    		$datas['count'] = $count[0]->count;
	    		$Fiche_client_link = site_url('fichier/?mode=menu_client&id_company='.$datas['id_client']);

	    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> <a href="'.$Fiche_client_link.'">'.$datas['nom_client'] . '</a> - Historique des appels';
	    		$datas['header_msg'].= ' ('.$datas['count'].')';

	    		// Récupération de la liste des contacts
	    		$datas['historique'] = $this->phoning_model->get_historique_rappel_list($id_user, $temp->id);	 	

		    	$this->load->view('header', $datas);
		    	$this->load->view('fichier/historique_taches', $datas);
		    	$this->load->view('footer', $datas);
		    	
		    break;

		    case 'historique_rdv':

	    		$datas['id_client'] = $this->input->get('id_client');
	    		$temp = $this->fichier_model->get_company_info($datas['id_client']);
	    		$datas['client'] = $temp;
	    		$datas['nom_client'] = $temp->nom_societe;

	    		// count
	    		$temps =  $this->phoning_model->get_count_tache_rdv($id_user, $temp->id, 'rdv');
	    		$count = $temps->result();
	    		$datas['count'] = $count[0]->count;

	    		$Fiche_client_link = site_url('fichier/?mode=menu_client&id_company='.$datas['id_client']);


	    		$datas['header_msg'] = '<i class="gen-enclosed icon-address-book"></i> <a href="'.$Fiche_client_link.'">'.$datas['nom_client'] . '</a> - Historique des RDV';
	    		$datas['header_msg'].= ' ('.$datas['count'].')';

	    		// Récupération de la liste des contacts
	    		$datas['historique'] = $this->phoning_model->get_historique_rdv_list($id_user, $temp->id);	

		    	$this->load->view('header', $datas);
		    	$this->load->view('fichier/historique_rdv', $datas);
		    	$this->load->view('footer');
		    	
		    break;

	    endswitch;
	}	

	public function record_fichier()
	{

		$id_user = $this->session->userdata('id');

		$DatasInsert = array(
		   'id_user'				=> $id_user,
		   'id_tranche_effectif' 	=> $this->input->post('id_tranche_effectif'),
		   'id_entite' 				=> $this->input->post('id_entite'),
		   'id_qualif'				=> $this->input->post('id_qualif'),
		   'id_zone_geo'			=> $this->input->post('id_zone_geo'),
		   'nom_societe'			=> $this->input->post('nom_societe'),
		   'nom_groupe'				=> $this->input->post('nom_groupe'),
		   'famille_activite'		=> $this->input->post('famille_activite'),
		   //'sous_activite'		=> $this->input->post('sous_activite'),
		   'type'					=> $this->input->post('type'),
		   'marque'					=> $this->input->post('marque'),
		   'adresse'				=> $this->input->post('adresse'),
		   'complement_adresse'		=> $this->input->post('complement_adresse'),
		   'cp'						=> $this->input->post('cp'),
		   'ville'					=> $this->input->post('ville'),
		   'telephone'				=> $this->input->post('telephone'),
		   'fax'					=> $this->input->post('fax'),
		   'site_internet'			=> $this->input->post('site_internet'),
		   'siret'					=> $this->input->post('siret'),
		   'siren'					=> $this->input->post('siren'),
		   'naf'					=> $this->input->post('naf'),
		   'comments'				=> $this->input->post('comments')
		);



		$this->db->insert('entreprises', $DatasInsert); 	

		// Redirection 
		redirect(site_url().'fichier/?mode=fiche_client&action=update&check=true&id_client='.$this->db->insert_id());
		$this->view();
	}

	public function update_fichier()
	{

		$id_user = $this->session->userdata('id');

		$DatasUpdate = array(
		   'id_user'				=> $id_user,
		   'id_tranche_effectif' 	=> $this->input->post('id_tranche_effectif'),
		   'id_entite' 				=> $this->input->post('id_entite'),
		   'id_qualif'				=> $this->input->post('id_qualif'),
		   'id_zone_geo'			=> $this->input->post('id_zone_geo'),
		   'nom_societe'			=> $this->input->post('nom_societe'),
		   'nom_groupe'				=> $this->input->post('nom_groupe'),
		   'famille_activite'		=> $this->input->post('famille_activite'),
		   //'sous_activite'		=> $this->input->post('sous_activite'),
		   'type'					=> $this->input->post('type'),
		   'marque'					=> $this->input->post('marque'),
		   'adresse'				=> $this->input->post('adresse'),
		   'complement_adresse'		=> $this->input->post('complement_adresse'),
		   'cp'						=> $this->input->post('cp'),
		   'ville'					=> $this->input->post('ville'),
		   'telephone'				=> $this->input->post('telephone'),
		   'fax'					=> $this->input->post('fax'),
		   'site_internet'			=> $this->input->post('site_internet'),
		   'siret'					=> $this->input->post('siret'),
		   'siren'					=> $this->input->post('siren'),
		   'naf'					=> $this->input->post('naf'),
		   'comments'				=> $this->input->post('comments')
		);


		$this->db->where('id', $this->input->post('id_client'));
		$this->db->update('entreprises', $DatasUpdate); 

		// Redirection 
		redirect(site_url().'fichier/?mode=fiche_client&action=update&id_client='.$this->input->post('id_client'));
	}	

	public function update_contact()
	{

		$id_user = $this->session->userdata('id');

		// Gestion du flag "Principal"
		if ($this->input->post('principal'))
			$Principal = 1;
		else
			$Principal = 0;

		$BirthdayTemp = explode('/', $this->input->post('birthday'));
		$Birthday = $BirthdayTemp[0].'/'.$BirthdayTemp[1];


		$DatasUpdate = array(
		   'id_entreprise'	=> $this->input->post('id_entreprise'),
		   'principal'	 	=> $Principal,
		   'civilite' 		=> $this->input->post('civilite'),
		   'nom' 			=> $this->input->post('nom'),
		   'prenom'			=> $this->input->post('prenom'),
		   'telephone'		=> $this->input->post('telephone'),
		   'gsm'			=> $this->input->post('gsm'),
		   'email'			=> $this->input->post('email'),
		   'fonction'		=> $this->input->post('fonction'),
		   'birthday'		=> $Birthday,
		   'viadeo'			=> $this->input->post('viadeo')
		);


		
		// Pour enlever l'autre contact principal
		if ($this->input->post('principal')):
			$DatasUpdateOther = array(
			   'principal'	=> '0'
			);
			$this->db->where('id_entreprise', $this->input->post('id_entreprise'));
			$this->db->update('contacts', $DatasUpdateOther); 
		endif;



		$this->db->where('id', $this->input->post('id_contact'));
		$this->db->update('contacts', $DatasUpdate); 

		// Redirection 
		redirect(site_url().'fichier/?mode=list_contacts&id_client='.$this->input->post('id_entreprise'));
	}	

	public function record_contact()
	{

		$id_user = $this->session->userdata('id');

		// Gestion du flag "Principal"
		if ($this->input->post('principal'))
			$Principal = 1;
		else
			$Principal = 0;


		$BirthdayTemp = explode('/', $this->input->post('birthday'));
		$Birthday = $BirthdayTemp[0].'/'.$BirthdayTemp[1];

		$DatasInsert = array(
		   'id_entreprise'	=> $this->input->post('id_entreprise'),
		   'principal'	 	=> $Principal,
		   'civilite' 		=> $this->input->post('civilite'),
		   'nom' 			=> $this->input->post('nom'),
		   'prenom'			=> $this->input->post('prenom'),
		   'telephone'		=> $this->input->post('telephone'),
		   'gsm'			=> $this->input->post('gsm'),
		   'email'			=> $this->input->post('email'),
		   'fonction'		=> $this->input->post('fonction'),
		   'birthday'		=> $Birthday,
		   'viadeo'			=> $this->input->post('viadeo')
		);



		$this->db->insert('contacts', $DatasInsert); 	

		// Redirection 
		redirect(site_url().'fichier/?mode=list_contacts&id_client='.$this->input->post('id_entreprise'));
	}

}

/* End of file fichier.php */
/* Location: ./application/controllers/fichier.php */