<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entreprises extends CI_Controller {

    function __construct()
    {   
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');

        $this->load->model('entreprises_model');
        $this->load->model('user_model');

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');


                
        if (!user_logged_in() && current_url() != site_url('user/login'))
        {
            redirect(site_url('user/login'));
        }
    }

	public function index()
	{
		$datas = array();
		$id_user = $this->session->userdata('id');

		$query = $this->entreprises_model->search_entreprise($this->input->get('query'), $id_user);

		$this->output
		    ->set_content_type('application/json')
		    ->set_output(json_encode($query->result()));
	}
	
	public function view()
	{

	}   	

	public function search_entreprise($string)
	{
		$datas = array();
		$id_user = $this->session->userdata('id');
		
		$datas['entreprises'] = $this->entreprises_model->search_entreprise($string,  $id_user);
		echo json_encode($datas['entreprises']);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/json/entreprises.php */