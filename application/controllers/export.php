<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

    function __construct()
    {   
        parent::__construct();



        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->library('form_validation');
        


        $this->load->model('phoning_model');
        $this->load->model('taches_model');
       	$this->load->model('docs_model'); 
        $this->load->model('user_model');
        $this->load->model('export_model');
        $this->load->model('fichier_model');

		$this->load->dbutil();
        
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');
        $this->load->helper('file');



        if (!user_logged_in() && current_url() != site_url('user/login'))
        {
            redirect(site_url('user/login'));
        }

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    	switch (@$this->input->get('search')):
	    		case 'speed': $this->speed_search(); break;
	    		case 'advanced': $this->advanced_search(); break;
	    		case 'objection': $this->objection(); break;
	    		case 'import': $this->import(); break;
	    		default: $this->view(); break;
	    	endswitch;
	}
	
	public function view()
	{
	    $datas = array();
	    
	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-inbox"></i> Exports';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;

    	$this->load->view('header', $datas);
    	$this->load->view('export/menu', $datas);
    	$this->load->view('footer');	    
	}

	public function speed_search()
	{
	    $datas = array();

	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = 'Bonjour ' . $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-inbox"></i> Recherche rapide <span></span>';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;

	    if ($this->input->post('search')):
	    				
			$string = $this->input->post('stringsearch');

			$datas['result'] = $this->export_model->do_search($string);
			$datas['query'] = $this->db->last_query();

			$temp = $datas['result']->result();
			$datas['total'] = count($temp);

	    elseif ($this->input->get('export') == 'true'):
				    	
	    	$request = $this->input->post('query');
			$query = $this->db->query($request);
	    	$temp = $this->export_model->xls($query);

	    endif;

    	$this->load->view('header', $datas);
    	$this->load->view('export/speed_search', $datas);
    	$this->load->view('footer');	    
	}

	public function advanced_search()
	{
	    $datas = array();

	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = 'Bonjour ' . $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-inbox"></i> Recherche avancée <span></span>';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;

		// Liste des familles
		$datas['list_famille'] = $this->fichier_model->get_list_famille();

		// Liste des commerciaux
		$datas['list_commercial'] = $this->fichier_model->get_list_commercial();

		// Liste des effectifs
		$datas['list_effectif'] = $this->fichier_model->get_list_effectif();

		// Liste des effectifs
		$datas['list_qualif'] = $this->fichier_model->get_list_qualif();

		// Liste des effectifs
		$datas['list_zones_geo'] = $this->fichier_model->get_list_zones_geo();

		// Liste des entité
		$datas['list_entite'] = $this->fichier_model->get_list_entite();

	    if ($this->input->post('search')):
	    				
			// Récupération des données
			$datas['ville']  			=  $ville  		= 	$this->input->post('ville');
			$datas['societe']  			=  $societe		= 	$this->input->post('societe');
			$datas['groupe'] 		 	=  $groupe 		= 	$this->input->post('groupe');
			$datas['marque']  			=  $marque 		= 	$this->input->post('marque');
			$datas['famille_activite']  =  $id_famille	= 	$this->input->post('famille_activite');
			$datas['type']  			=  $type 		= 	$this->input->post('type');
			$datas['id_contact']  		=  $id_contact 	= 	$this->input->post('id_contact');
			$datas['id_entite']			=  $id_entite	=	$this->input->post('id_entite');
			$datas['id_zone_geo']  		=  $id_zone		= 	$this->input->post('id_zone_geo');
			$datas['email']  			=  $this->input->post('email');


			if ($datas['email'] <> '')
				$IsEmail = true;
			else
				$IsEmail = false;

			$datas['advanced_result'] = $this->export_model->do_advanced_search($ville, $societe, $groupe, $marque, $id_famille, $type, $id_contact, $id_zone, $IsEmail, $id_user, $id_entite);
			
			$tempQuery = $this->db->last_query();

			$datas['query'] = str_replace(' COUNT(*) as total, e.id, ', ' ', $tempQuery);

			$temp = $datas['advanced_result']->result();
			$datas['total'] = count($temp);



	    elseif ($this->input->get('export') == 'true'):
				    	
	    	$request = $this->input->post('query');

			$query = $this->db->query($request);

	    	$temp = $this->export_model->xls($query);

	    endif;


    	$this->load->view('header', $datas);
    	$this->load->view('export/advanced_search', $datas);
    	$this->load->view('footer');	
	}	

	public function objection()
	{
	    $datas = array();

	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = 'Bonjour ' . $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-inbox"></i> Les fiches ayant une objection <span></span>';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);
	    $datas['logo_entite'] = $datas['entite']->logo;
	    
		$datas['result'] = $this->export_model->get_objection_list($id_user);
		$tempQuery = $this->db->last_query();

		$datas['query'] = str_replace(' COUNT(*) as total, e.id, ', ' ', $tempQuery);


	    if ($this->input->get('export') == 'true'):
				    	
	    	$request = $this->input->post('query');

			$query = $this->db->query($request);

	    	$temp = $this->export_model->xls($query);

	    endif;

    	$this->load->view('header', $datas);
    	$this->load->view('export/objection', $datas);
    	$this->load->view('footer');	    		
	}

	public function import()
	{
		$this->load->library('csv_reader');
		$result = $this->csv_reader->parseFile(site_url().'uploads/Familles.csv');

		$data['csvData'] =  $result;

		foreach($result as $values):
			$tmp = explode('-', $values);
			$famille =  preg_replace("/(\r\n|\n|\r)/", " ", $tmp[1]);
			$data = array(
               'famille_activite' => $famille,
            );
/*
			$this->db->where('id', $tmp[0]);
			$this->db->update('entreprises', $data); 

			echo $this->db->last_query().'<br />';
*/

		endforeach;


		$this->load->view('export/import', $data);  		
	}
}

/* End of file docs.php */
/* Location: ./application/controllers/welcome.php */