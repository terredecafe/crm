<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taches extends CI_Controller {

    function __construct()
    {   
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('phoning_model');
        $this->load->model('taches_model');
        $this->load->model('user_model');

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('convdate');

        
        if (!user_logged_in() && current_url() != site_url('user/login'))
        {
            redirect(site_url('user/login'));
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    if ($this->input->post()):

	    	switch ($this->input->post('type')):
	    		case 'add': $this->record(); break;
	    		case 'update': $this->update(); break;
	    		case 'create': $this->record(); break;
	    	endswitch;

	    else:
	    	$this->view();
	    endif;
	    
	}
	
	public function view()
	{
	    $datas = array();
	    
	    $id_user = $this->session->userdata('id');
	    $datas['user'] = $this->user_model->get_user_datas($id_user);
	    $datas['welcome'] = $datas['user']->prenom . ' ' . $datas['user']->nom;   
	    $datas['header_msg'] = '<i class="gen-enclosed icon-checkmark"></i> Mes Tâches';
	    $datas['entite'] = $this->user_model->get_logo_entite($datas['user']->id_entite);

	    $datas['logo_entite'] = $datas['entite']->logo;

	    $datas['results'] = $this->taches_model->get_list($id_user);


	    // CLOTURE = SUPPRESION !!
	    if (@$this->input->get('cloture')):
	    	$this->taches_model->cloturer($this->input->get('id'));
	    	redirect(site_url().'taches');
	    endif;
	    
	    switch ($this->input->get('mode')):

	    	default: // LIST
		    	$this->load->view('header', $datas);
		    	$this->load->view('taches/list', $datas);
		    	$this->load->view('footer');
	    	break;

	    	case 'create': // CREATION
	    		$datas['header_msg'] = '<i class="gen-enclosed icon-checkmark"></i> Cr&eacute;ation d\'une nouvelle t&acirc;che';

	    		// Cas particulier pour TDC / Virginie
	    		if ($id_user == '22'):
	    			$datas['list_company'] = $this->taches_model->get_list_company_virginie();
	    		else:
	    			$datas['list_company'] = $this->taches_model->get_list_company($id_user);
	    		endif;

	    		// Récupération de la liste des types
	    		$datas['type_list'] = $this->taches_model->get_type_list($datas['user']->id_entite);



	    		/* Chargement des vues */
		    	$this->load->view('header', $datas);
		    	$this->load->view('taches/create', $datas);
		    	$this->load->view('footer');	    		
	    	break;

	    	case 'update': // MODIFICATION   		

	    		// Récupération des informations de la ligne de phoning concernée
	    		$datas['id_tache'] = $this->input->get('id');

	    		$Datas = $this->taches_model->get_tache($datas['id_tache']);
	    		$datas['infos_tache'] = $Datas->result();
	    		$datas['infos_tache']  = $datas['infos_tache'][0];
	    		$datas['list_company'] = $this->taches_model->get_list_company($datas['user']->id_entite);

	    		$datas['type_list'] = $this->taches_model->get_type_list($datas['user']->id_entite);


	    		$datas['header_msg'] = '<i class="gen-enclosed icon-checkmark"></i> Modification de la t&acirc;che #'.$datas['id_tache'];

		    	$this->load->view('header', $datas);
		    	$this->load->view('taches/create', $datas);
		    	$this->load->view('footer');	    		
	    	break;

	    endswitch;
	}

	public function record()
	{

		$id_user = $this->session->userdata('id');

		$DatasInsert = array(
		   'id_user'			=> $id_user,
		   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
		   'id_entreprise' 		=> $this->input->post('company'),
		   'commentaire' 		=> $this->input->post('commentaires'),
		   'type'				=> $this->input->post('type_tache'),
		   'cloture' 			=> $this->input->post('cloture')
		   
		);

		$this->db->insert('taches', $DatasInsert); 	
		
		// Redirection 
		$this->view();
	}

	public function update()
	{

		$id_user = $this->session->userdata('id');

		
		if($this->input->post('cloture')): // Vérification si CLOTURE, donc on efface la ligne
			
			$this->taches_model->delete($this->input->post('id_tache'));

			// Redirection
			redirect(site_url().'taches');

		else:

			$DatasUpdate = array(
			   'id_user'			=> $id_user,
			   'date' 				=> date_fr_to_us($this->input->post('date'), '00:00:00'),
			   'id_entreprise' 		=> $this->input->post('company'),
			   'commentaire' 		=> $this->input->post('commentaires'),
			   'type'				=> $this->input->post('type_tache'),
			   'cloture' 			=> '0'
			);


			$this->db->where('id', $this->input->post('id_tache'));
			$this->db->update('taches', $DatasUpdate); 
				
			// Redirection 
			redirect(site_url().'taches/?mode=update&id='.$this->input->post('id_tache'));

		endif;
	}	    	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */