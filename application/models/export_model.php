<?php

class Export_Model extends CI_Model  {

    function __construct()
    {
    
    }


    function do_search($string)
    {
        
        $query = $this->db->query('SELECT COUNT(*) as total, e.id, e.nom_societe, e.type, c.civilite, c.nom, c.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                            FROM `entreprises` as e
                            LEFT JOIN `contacts` AS c
                            ON e.id = c.id_entreprise
                            LEFT JOIN `familles` as f
                            ON e.famille_activite = f.id
                            LEFT JOIN `users` as u
                            ON u.id = e.id_user
                            WHERE (e.nom_groupe LIKE  "%'.$string.'%" 
                            OR e.nom_societe LIKE  "%'.$string.'%"
                            OR e.ville LIKE "%'.$string.'%" )
                            AND e.status = "PUBLISHED"
                            GROUP BY e.id ');
                                     
        return $query;                           
    }

    function simple_search($string)
    {
        $query = $this->db->query('SELECT e.id, e.nom_societe, e.type, c.civilite, c.nom, c.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                            FROM `entreprises` as e
                            LEFT JOIN `contacts` AS c
                            ON e.id = c.id_entreprise
                            LEFT JOIN `familles` as f
                            ON e.famille_activite = f.id
                            LEFT JOIN `users` as u
                            ON u.id = e.id_user
                            WHERE (e.nom_groupe LIKE  "%'.$string.'%" 
                            OR e.nom_societe LIKE  "%'.$string.'%")
                            AND e.status = "PUBLISHED"
                            GROUP BY e.id ');
                                     
        return $query;
    }

    function simple_search_before_doublon($string, $id_e)
    {
        $query = $this->db->query('SELECT e.id, e.nom_societe, e.type, c.civilite, c.nom, c.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                            FROM `entreprises` as e
                            LEFT JOIN `contacts` AS c
                            ON e.id = c.id_entreprise
                            LEFT JOIN `familles` as f
                            ON e.famille_activite = f.id
                            LEFT JOIN `users` as u
                            ON u.id = e.id_user
                            WHERE (e.nom_groupe LIKE  "%'.$string.'%" 
                            OR e.nom_societe LIKE  "%'.$string.'%")
                            AND e.status = "PUBLISHED"
                            AND e.id <> '.$id_e.'
                            GROUP BY e.id ');
                                     
        return $query;
    }

    function simple_search_doublon($string, $minlength=4)
    {

        $string_parts = explode(' ', $string);

        $search_group = array();
        $search_societe = array();

        foreach($string_parts as $string_part) {
            if (strlen($string_part) >= $minlength) {
                $search_group[] = "e.nom_groupe LIKE  \"%$string_part%\"";
                $search_societe[] = "e.nom_societe LIKE  \"%$string_part%\"";
            }
        }

        if(empty($search_group)):

            $query = $this->db->query('SELECT e.id, e.nom_societe, e.type, c.civilite, u.nom, u.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                                FROM `entreprises` as e
                                LEFT JOIN `contacts` AS c
                                ON e.id = c.id_entreprise
                                LEFT JOIN `familles` as f
                                ON e.famille_activite = f.id
                                LEFT JOIN `users` as u
                                ON u.id = e.id_user
                                WHERE (e.nom_groupe LIKE "%'.$search_group.'%"
                                OR e.nom_societe LIKE "%'.$search_societe.'%")
                                AND e.status = "PUBLISHED"
                                GROUP BY e.id');

        else:


            $query = $this->db->query('SELECT e.id, e.nom_societe, e.type, c.civilite, u.nom, u.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                                FROM `entreprises` as e
                                LEFT JOIN `contacts` AS c
                                ON e.id = c.id_entreprise
                                LEFT JOIN `familles` as f
                                ON e.famille_activite = f.id
                                LEFT JOIN `users` as u
                                ON u.id = e.id_user
                                WHERE ('.implode(' OR ', $search_group).' 
                                OR '.implode(' OR ', $search_societe).') 
                                AND e.status = "PUBLISHED"
                                GROUP BY e.id');
        endif;
                                     
        return $query;
    }


    function do_advanced_search($ville, $societe, $groupe, $marque, $id_famille, $type, $id_commercial, $id_zone, $IsEmail, $id_user, $id_entite)
    {

        $request = 'SELECT COUNT(*) as total, u.nom as u_nom, u.prenom as u_prenom, e.id,  e.nom_societe, e.type, c.civilite, c.nom, c.prenom, c.telephone, c.gsm, c.email, c.fonction, e.ville, f.nom_famille, e.adresse, e.cp 
                            FROM `entreprises` as e
                            LEFT JOIN `contacts` AS c
                            ON e.id = c.id_entreprise
                            LEFT JOIN `familles` as f
                            ON e.famille_activite = f.id  
                            LEFT JOIN `users` as u
                            ON u.id = e.id_user                          
                            WHERE e.id <> -2 AND e.status = "PUBLISHED"';

        if ($societe != '')
            $request .= ' AND e.nom_societe LIKE  "%'.$societe.'%"';

        if ($ville != '')
            $request .= ' AND e.ville LIKE  "%'.$ville.'%"';

        if ($groupe != '')
            $request .= ' AND e.nom_groupe LIKE  "%'.$groupe.'%"';     
        
        if ($marque != '')
            $request .= ' AND e.marque LIKE  "%'.$marque.'%"'; 
            
        if ($id_famille != '')
            $request .= ' AND e.famille_activite = '.$id_famille; 
            
        if ($type != '')
            $request .= ' AND e.type LIKE "%'.$type.'%"';         
        
        if ($id_commercial != '' AND $id_commercial != 'all')
            $request .= ' AND e.id_user = '.$id_commercial;                                 

        if ($id_zone != '')
            $request .= ' AND e.id_zone_geo = '.$id_zone; 
            
        if ($id_entite != '' AND $id_entite != 'all'):
            if ($id_entite == 'F1P')
                $request .= ' AND (e.id_entite =  2 OR e.id_entite = 3) '; 
            else
                $request .= ' AND e.id_entite = '.$id_entite; 
        endif;                               

        if ($IsEmail)
            $request .= " AND c.email <> '' ";

        $request .= ' GROUP BY e.id ';      
        //echo $request;
        $query = $this->db->query($request);
        
        //echo $request;           
        return $query;        
    }

    function get_objection_list($IDUser)
    {
        $query = $this->db->query('SELECT p.objection,e.id, e.nom_societe, e.type, c.civilite, c.nom, c.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                            FROM `entreprises` as e
                            LEFT JOIN `contacts` AS c
                            ON e.id = c.id_entreprise
                            LEFT JOIN `familles` as f
                            ON e.famille_activite = f.id
                            LEFT JOIN `phoning` as p
                            ON p.id_entreprise = e.id
                            LEFT JOIN `users` as u
                            ON u.id = e.id_user
                            WHERE p.objection <> 0
                            AND e.id_user = '.$IDUser.'
                            AND e.status = "PUBLISHED"
                            GROUP BY e.id ');
                                     
        return $query;         
    }

    function xls($query)
    {
        
        if(!$query)
            return false;
        
        // Starting the PHPExcel library
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        
        foreach ($fields as $field):
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        endforeach;
        // Fetching the table data
        $row = 2;
        foreach($query->result() as $data):
            $col = 0;
            foreach ($fields as $field):
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, (string)$data->$field);
                $col++;
            endforeach;
            $row++;
        endforeach;

        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Recherche_'.date('d-M-y').'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');        
    }
}