<?php

class Entreprises_Model  extends CI_Model  {

    function __construct()
    {
    
    }

    function search_entreprise($string, $user_id)
    {
        
        
        /*$this->db->select('id, nom_societe, ville');
        $this->db->from('entreprises');
        $this->db->like('nom_societe', $string); 
        $this->db->or_like('nom_groupe', $string); 
        $this->db->or_like('marque', $string); 
        */

        $Query = $this->db->query('SELECT 
                                    entreprises.id as id,
                                    entreprises.nom_societe as nom_societe,  
                                    entreprises.ville as ville,
                                    contacts.nom as nom,
                                    contacts.prenom as prenom
                                    FROM `entreprises` as entreprises 
                                    LEFT JOIN `contacts` AS contacts 
                                    ON entreprises.id = contacts.id_entreprise 
                                    WHERE (
                                        entreprises.nom_groupe  LIKE "%'.$string.'%"
                                        OR entreprises.marque LIKE "%'.$string.'%"
                                        OR contacts.nom LIKE "%'.$string.'%"
                                        )
                                    AND entreprises.id_user = '.$user_id.'
                                    AND entreprises.status = "PUBLISHED"
                                    GROUP BY id');  
        return $Query;  
    }

}