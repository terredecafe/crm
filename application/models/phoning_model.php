<?php

class Phoning_Model  extends CI_Model  {

    function __construct()
    {
        
    }

    function get_list($id_user)
    {

        /*
        $this->db->select('*');
        $this->db->from('phoning');
        $this->db->join('contacts', 'contacts.id = phoning.id_contact');
        $this->db->join('entreprises', 'contacts.id_entreprise = entreprises.id');
        $this->db->where('phoning.id_user', $id_user);

        return $Query = $this->db->get_where('phoning', array('id_user' => $id_user));

        */

        $iTomorrow = time () + 86400;
        $SQLdate = date ('Y-m-d', $iTomorrow);


        $query = $this->db->query('SELECT 
                                        phoning.id as p_id, 
                                        phoning.id_user as p_id_user,
                                        phoning.date as p_date,
                                        phoning.result_rdv_date as p_result_rdv_date,
                                        phoning.result_rappel_date as p_result_rappel_date,
                                        phoning.result_rdv as p_result_rdv,
                                        phoning.result_rappel as p_result_rappel,
                                        phoning.commentaire as p_commentaire,
                                        phoning.cloture as p_cloture,
                                        phoning.objection as p_objection,
                                        contacts.id as c_id,
                                        contacts.nom as n_nom,
                                        contacts.prenom as n_prenom,
                                        contacts.telephone as n_telephone,
                                        contacts.gsm as n_gsm,
                                        entreprises.nom_societe as e_nom_societe,
                                        entreprises.id as id_client
                                        FROM `phoning` as phoning
                                        LEFT JOIN `contacts` AS contacts ON contacts.id = phoning.id_contact
                                        LEFT JOIN `entreprises` AS entreprises ON entreprises.id = contacts.id_entreprise
                                        WHERE phoning.id_user ='.$id_user.'
                                        AND result_rappel_date < "'.$SQLdate.'"
                                        AND result_rdv = 0
                                        AND phoning.cloture = 0
                                        ORDER BY result_rappel_date ASC');         
        return $query;
    }

    function get_historique_list($id_user, $id_entreprise)
    {

        $Now = date('Y-m-d');
        
        $query = $this->db->query('SELECT 
                                    phoning.id as t_id,
                                    phoning.date as t_date,  
                                    phoning.result_rdv as t_result_rdv, 
                                    phoning.result_rdv_date as t_result_rdv_date,
                                    phoning.commentaire as t_commentaire, 
                                    phoning.objection as t_objection, 
                                    contacts.nom as c_nom, 
                                    contacts.prenom as c_prenom
                                    FROM `phoning` as phoning
                                    LEFT JOIN `contacts` AS contacts
                                    ON contacts.id_entreprise = phoning.id_entreprise
                                    WHERE phoning.cloture = 1
                                     /* AND phoning.id_user ='.$id_user.' */
                                    AND phoning.id_entreprise ='.$id_entreprise.'
                                    GROUP BY t_id');
                                     
        return $query;
    }


    function get_historique_rappel_list($id_user, $id_entreprise)
    {

        $Now = date('Y-m-d');
        
        $query = $this->db->query('SELECT 
                                    phoning.id as t_id,
                                    phoning.date as t_date,  
                                    phoning.result_rdv as t_result_rdv, 
                                    phoning.result_rdv_date as t_result_rdv_date,
                                    phoning.result_rappel_date  as t_result_rappel_date, 
                                    phoning.commentaire as t_commentaire, 
                                    phoning.objection as t_objection, 
                                    contacts.id as c_id, 
                                    contacts.nom as c_nom, 
                                    contacts.prenom as c_prenom
                                    FROM `phoning` as phoning
                                    LEFT JOIN `contacts` AS contacts
                                    ON contacts.id_entreprise = phoning.id_entreprise
                                    WHERE /* phoning.id_user ='.$id_user.' 
                                    AND */ phoning.id_entreprise ='.$id_entreprise.'
                                    AND (phoning.result_rappel = 1 OR phoning.result_rdv = 1)
                                    GROUP BY t_id
                                    ORDER BY t_result_rdv_date DESC, t_result_rappel_date DESC');

                                     
        return $query;
    }

    function get_historique_rdv_list($id_user, $id_entreprise)
    {

        $Now = date('Y-m-d');
        
        $query = $this->db->query('SELECT 
                                    phoning.id as t_id,
                                    phoning.date as t_date,  
                                    phoning.result_rdv as t_result_rdv, 
                                    phoning.result_rdv_date as t_result_rdv_date,
                                    phoning.commentaire as t_commentaire, 
                                    phoning.objection as t_objection, 
                                    contacts.id as c_id,
                                    contacts.nom as c_nom, 
                                    contacts.prenom as c_prenom
                                    FROM `phoning` as phoning
                                    LEFT JOIN `contacts` AS contacts
                                    ON contacts.id_entreprise = phoning.id_entreprise
                                    WHERE /* phoning.id_user ='.$id_user.'
                                    AND */ phoning.id_entreprise = '.$id_entreprise.'
                                    AND phoning.result_rdv = 1 
                                    GROUP BY t_id
                                    ORDER BY t_result_rdv_date DESC');

        
        return $query;
    }    

    function get_count_tache($id_user, $id_entreprise, $type)
    {

        if ($type == 'rdv'):
            $string = ' AND phoning.result_rdv = 1 ';
        elseif($type == 'rappel'):
            $string = ' AND phoning.result_rappel = 1 ';
        endif;

        $query = $this->db->query(' SELECT 
                                    count(*) as count
                                    FROM `phoning` as phoning
                                    WHERE /* phoning.id_user ='.$id_user.'
                                    AND */ phoning.id_entreprise ='.$id_entreprise.'
                                    AND (phoning.result_rappel = 1 OR phoning.result_rdv = 1)
                                    ');        
    return $query;                                     

    }

    function get_count_tache_rdv($id_user, $id_entreprise, $type)
    {

        if ($type == 'rdv'):
            $string = ' AND phoning.result_rdv = 1 ';
        elseif($type == 'rappel'):
            $string = ' AND phoning.result_rappel = 1 ';
        endif;

        $query = $this->db->query(' SELECT 
                                    count(*) as count
                                    FROM `phoning` as phoning
                                    WHERE /* phoning.id_user ='.$id_user.'
                                    AND */ phoning.id_entreprise ='.$id_entreprise.'
                                    AND phoning.result_rdv = 1
                                    ');        
    return $query;                                     

    }

    function get_company_info($id_comp)
    {
        return $Query = $this->db->get_where('entreprises', array('id' => $id_comp))->row(0);
    }

    function get_contact_infos($id_contact)
    {
    	return $Query = $this->db->get_where('contacts', array('id' => $id_contact))->row(0);
    }

    function get_list_contact_from_client($id_client)
    {
        return $Query = $this->db->get_where('contacts', array('id' => $id_client));
    }

    function get_phoning_info($id_phoning)
    {
        return $Query = $this->db->get_where('phoning', array('id' => $id_phoning))->row(0);
    }

    function create_google_event($summary, $description, $location, $start_date, $start_time, $end_date, $email)
    {
        // Calcul du END TIME
        $temp = explode(':', $start_time);
        $hour = $temp[0];
        $end_time = $hour+1;

        if (@$temp[1] == '00')
            $end_time .= ':00';
        else
            $end_time .= ':'.@$temp[1];


        $start_date = str_replace(' ', '', $start_date);
        $end_date = str_replace(' ', '', $end_date);
        $summary = str_replace(' ', '_', $summary);

        $params = '?auth=true&Summary='.$summary.'&Description='.$description.'&Location='.$location.'&start_date='.$start_date.'&start_time='.$start_time.'&end_date='.$end_date.'&end_time='.$end_time.'&email='.$email;

        $url = 'http://app.terredecafe.fr/api.google/Accueil';
        $link = $url.$params; 
        ?>
        <iframe height="1" width="1" src="<?php echo $link; ?>" style="visibility: hidden;"></iframe> 
        <?

        return $link;
    }

}


