<?php

class Docs_Model extends CI_Model  {

    function __construct()
    {
    
    }

    function get_type_list($id_entite)
    {

        if ($id_entite == '1') :

            $options_type = array(
                'Reco'  => 'Reco',
                'Devis' => 'Devis',
                'Creations' => 'Créations',
                'CDC' => 'Cahier des charges'
            );

        elseif($id_entite == '2'):

            $options_type = array(
            'PMO'  =>  'PMO',
            'Devis' =>  'Devis',
            'Spots' =>  'Spots'
            );

        elseif($id_entite == '3'):

            $options_type = array(
            'PMO'  =>  'PMO',
            'Devis' =>  'Devis',
            'Spots' =>  'Spots'
            );
        elseif($id_entite == '4'):

            $options_type = array(
            'Reco'  =>  'Reco',
            'Devis' =>  'Devis',
            'Spots' =>  'Spots'
            );
        endif;

        
        return $options_type;        
    }


    function get_list($id_user)
    {
        
        $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                            FROM `docs` as docs
                            LEFT JOIN `entreprises` AS societe
                            ON societe.id = docs.id_entreprise
                            WHERE docs.id_user ='.$id_user.'
                            AND societe.status = "PUBLISHED"
                            ORDER BY date DESC
                            ');
                                     
        return $query;              
    }

    function get_list_by_client($id_user, $CompanyID)
    {
        
        /*$query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                            FROM `docs` as docs
                            LEFT JOIN `entreprises` AS societe
                            ON societe.id = docs.id_entreprise
                            WHERE docs.id_user ='.$id_user.'
                            AND docs.id_entreprise ='.$CompanyID.'
                            AND societe.status = "PUBLISHED"
                            ORDER BY date DESC
                            ');*/

        $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                            FROM `docs` as docs
                            LEFT JOIN `entreprises` AS societe
                            ON societe.id = docs.id_entreprise
                            WHERE docs.id_entreprise ='.$CompanyID.'
                            AND societe.status = "PUBLISHED"
                            ORDER BY date DESC
                            ');


                                     
        return $query;              
    }


    function get_list_w_filter($id_user, $filter)
    {
        
        if($filter == 'all'):
            $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                                        FROM `docs` as docs
                                        LEFT JOIN `entreprises` AS societe
                                        ON societe.id = docs.id_entreprise
                                        WHERE docs.id_user ='.$id_user.'
                                        ORDER BY date DESC
                                        ');
        else:
            $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                                        FROM `docs` as docs
                                        LEFT JOIN `entreprises` AS societe
                                        ON societe.id = docs.id_entreprise
                                        WHERE docs.id_user ='.$id_user.'
                                        AND docs.type ="'.$filter.'"
                                        ORDER BY date DESC
                                        ');
        endif;


                                     
        return $query;              
    }    


    function get_list_w_filter_by_client($id_user, $filter, $CompanyID)
    {
        
        if($filter == 'all'):
            $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                                        FROM `docs` as docs
                                        LEFT JOIN `entreprises` AS societe
                                        ON societe.id = docs.id_entreprise
                                        WHERE docs.id_user ='.$id_user.'
                                        AND docs.id_entreprise ='.$CompanyID.'
                                        ORDER BY date DESC
                                        ');
        else:
            $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                                        FROM `docs` as docs
                                        LEFT JOIN `entreprises` AS societe
                                        ON societe.id = docs.id_entreprise
                                        WHERE docs.id_user ='.$id_user.'
                                        AND docs.type ="'.$filter.'"
                                        AND docs.id_entreprise ='.$CompanyID.'
                                        ORDER BY date DESC
                                        ');
        endif;


                                     
        return $query;              
    } 

    function get_doc($id_doc)
    {
        return $Query = $this->db->get_where('docs', array('id' => $id_doc))->row(0);
        
    }

    function get_doc_by_client($id_user, $id_client)
    {
        $query = $this->db->query('SELECT docs.date, docs.id, societe.nom_societe, docs.name, docs.type, docs.file
                            FROM `docs` as docs
                            LEFT JOIN `entreprises` AS societe
                            ON societe.id = docs.id_entreprise
                            WHERE docs.id_user ='.$id_user.'
                            AND societe.id = '.$id_client.'
                            ORDER BY date DESC
                            ');
                        
        return $query;           
    }

    function delete_doc($id)
    {
        return $this->db->delete('docs', array('id' => $id)); 
    }


/*
SELECT * FROM (`company`) 
LEFT JOIN `address_link` ON `company`.`Comp_CompanyId` = `address_link`.`AdLi_CompanyID` 
LEFT JOIN `person` ON `person`.`Pers_CompanyId` = `company`.`Comp_CompanyId` 
LIMIT 0,1000    

SELECT * FROM (`company`) 
JOIN `address_link` ON `company`.`Comp_CompanyId` = `address_link`.`AdLi_CompanyID` 
JOIN `person` ON `person`.`Pers_CompanyId` = `company`.`Comp_CompanyId` 
LIMIT 0,100  
.



mysql -u root -p"dm3x1ppF" ci_crm2012 -B -e "select * from JOHNNNNNN;" | sed 's/\t/","/g;s/^/"/;s/$/"/;s/\n//g' > test.csv

*/
}