<?php

class Fichier_Model  extends CI_Model  {

    function __construct()
    {
    
    }

    function get_list($id_user)
    {
        //return $Query = $this->db->get_where('entreprises', array('id_user' => $id_user));  
        return $Query = $this->db->from('entreprises')->where(array('id_user' => $id_user, 'status' => 'PUBLISHED'))->order_by('nom_societe', 'ASC')->get();
    }

    function get_doublon($Enseigne) 
    {
        return $Query = $this->db->from('entreprises')->where(array('nom_societe' => $Enseigne))->get();
    }

    function get_search_list($id_user) {

        $query = $this->db->query('SELECT 
                                    *
                                    FROM `entreprises` as e
                                    WHERE e.id_user ='.$id_user.'

                                    AND e.status = "PUBLISHED"
                                    ORDER BY e.nom_societe ASC');         
        return $query;        
    }

    function get_search_list_contact($id_user) {

        $query = $this->db->query('SELECT * FROM `contacts` as c WHERE c.principal = 1');         
        return $query;        
    }

    function get_list_count($id_user)
    {
        $this->db->select('COUNT(*) AS total');
        $this->db->from('entreprises');
        $this->db->where(array('id_user' => $id_user, 'status' => 'PUBLISHED'));

        return $Query = $this->db->get();   
    }

    function get_query_export($id_user)
    {
        $string = 'SELECT COUNT(*) as total, e.id, e.nom_societe, e.type, c.civilite, c.nom, c.prenom, c.telephone, c.gsm, c.email, e.ville, f.nom_famille, e.adresse, e.cp 
                    FROM `entreprises` as e
                    LEFT JOIN `contacts` AS c
                    ON e.id = c.id_entreprise
                    LEFT JOIN `familles` as f
                    ON e.famille_activite = f.id
                    LEFT JOIN `users` as u
                    ON u.id = e.id_user
                    WHERE e.status = "PUBLISHED" AND id_user='.$id_user;

        return $string;
    }

    function get_company_info($id_entreprise)
    {
        return $Query = $this->db->get_where('entreprises', array('id' => $id_entreprise))->row(0);
    }

    function get_entite_by_id($id_entite)
    {
        return $Query = $this->db->get_where('entite', array('id' => $id_entite))->row(0);
    }


    function get_list_famille() {
        return $Query = $this->db->get('familles');
    }

    function get_list_famille_by_entite($IDentite) {

        switch ($IDentite):
            case '1': $SQLwhere = ' id = 5 OR id = 20 OR id = 47 OR id > 55'; break; // TDC
            case '2': $SQLwhere = ' id < 56 '; break; // F1P LENS BETHUNE
            case '3': $SQLwhere = ' id < 56'; break; // F1P LILLE
            case '4': $SQLwhere = ' id < 58'; break; // 14h28
        endswitch;

        $query = $this->db->query('SELECT * FROM familles WHERE '.$SQLwhere);           
        return $query;
  

        //return $Query = $this->db->get('familles');
    }

    function get_list_commercial()
    {
        return $Query = $this->db->get('users');
    }

    function get_list_effectif()
    {
        return $Query = $this->db->get('tranches_effectif');
    }

    function get_list_qualif()
    {
        return $Query = $this->db->get('qualifications_entreprises');
    }
    function get_list_zones_geo()
    {
        return $Query = $this->db->get('zones_geo');
    }

    function get_list_zones_geo_by_entite($IDentite)
    {
        switch ($IDentite):
            case '1': $SQLwhere = ' id > 4 AND id < 11'; break; // TDC
            case '2': $SQLwhere = ' id < 5 OR id = 12'; break; // F1P LENS BETHUNE
            case '3': $SQLwhere = ' id = 11 OR id = 12 OR id = 13'; break; // F1P LILLE
            case '4': $SQLwhere = ' id > 9'; break; // 14h28
        endswitch;

        $query = $this->db->query('SELECT * FROM zones_geo WHERE '.$SQLwhere);           
        return $query;    
    }

    function get_list_fonctions_contact()
    {
        return $Query = $this->db->get('fonctions');
    }

    function get_list_fonctions_contact_by_entite($IDentite)
    {
        switch ($IDentite):
            case '1': $SQLwhere = ' id = 3 OR id > 7 AND ID <> 53'; break; // TDC
            case '2': $SQLwhere = ' id < 8'; break; // F1P LENS BETHUNE
            case '3': $SQLwhere = ' id < 8'; break; // F1P LILLE
            case '4': $SQLwhere = ' id = 2 OR id = 3 OR id = 4 OR id = 6 OR id = 40 OR id = 42 OR id = 48 OR id = 53'; break; // 14h28
        endswitch;

        $query = $this->db->query('SELECT * FROM fonctions WHERE '.$SQLwhere);  
        echo 'SELECT * FROM fonctions WHERE '.$SQLwhere;         
        return $query;    
    }

    function get_list_entite()
    {
        return $Query = $this->db->get('entite');
    }

    function get_list_contacts($id_entreprise)
    {
        $this->db->select('id, id_entreprise, principal, nom, prenom, telephone, gsm');
        $this->db->from('contacts');
        $this->db->where('id_entreprise', $id_entreprise);

        return $Query = $this->db->get();   
    }

    function get_first_contact_id($id_entreprise)
    {
        $this->db->select('*');
        $this->db->from('contacts');
        $this->db->where(array('id_entreprise' => $id_entreprise));
        $this->db->where(array('principal' => '1'));

        return $Query = $this->db->get();
    }
    
    function get_contact_info($id_contact)
    {
        return $Query = $this->db->get_where('contacts', array('id' => $id_contact))->row(0);
    }    

    function get_other_contact_list($id_entreprise, $not_in)
    {
        $this->db->select('id, nom, prenom');
        $this->db->from('contacts');
        $this->db->where('id_entreprise', $id_entreprise);
        $this->db->where('id !=', $not_in);

        return $Query = $this->db->get();        
    }

    function delete_fiche($id)
    {
        return $this->db->delete('entreprises', array('id' => $id)); 
    }

    function BirthdaySearch($id_entreprise)
    {
        $Today = date('d/m');

        $this->db->select('id, nom, prenom');
        $this->db->from('contacts');
        $this->db->where('id_entreprise', $id_entreprise);
        $this->db->where('birthday', $Today);

        return $Query = $this->db->get();     
    }

    function BirthdaySearchAll()
    {
        $Today = date('d/m');

        $this->db->select('id, id_entreprise, nom, prenom');
        $this->db->from('contacts');
        $this->db->where('birthday', $Today);

        return $Query = $this->db->get();     
    }

    function BirthdaySearchByUser($IDUser)
    {
        $Today = date('d/m');

        $query = $this->db->query("SELECT 
                                    c.id, c.id_entreprise, c.nom, c.prenom, e.nom_societe
                                    FROM `contacts` as c
                                    LEFT JOIN `entreprises` AS e ON c.id_entreprise = e.id
                                    WHERE e.id_user = ".$IDUser."
                                    AND c.birthday = '".$Today."'");        
        return $query;
    }
}