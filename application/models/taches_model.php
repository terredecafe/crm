<?php

class Taches_Model  extends CI_Model  {

    function __construct()
    {
    
    }

    function get_list($id_user)
    {
        /*
        $this->db->select('*');
        $this->db->from('taches');
        $this->db->join('entreprises', ' entreprises.id = taches.id_entreprise');    
        $this->db->where('taches.id_user = '.$id_user);
        */
        
        $query = $this->db->query('SELECT taches.date, taches.id, societe.nom_societe, taches.type, taches.commentaire, taches.cloture
                            FROM `taches` as taches
                            LEFT JOIN `entreprises` AS societe
                            ON societe.id = taches.id_entreprise
                            WHERE taches.cloture = 0 
                            AND taches.id_user ='.$id_user.'
                            ORDER BY date ASC');
                                     
        return $query;
    }

    function get_type_list($id_entite)
    {

        if ($id_entite == '1') :

            $options_type = array(
                'Brief-cdp'  =>  'Brief / Chef de Projet',
                'Analytics' => 'Rapport Analytics',
                'Livraison' => 'Livraison projet',
                'Suivi' => 'Suivi de projet',
                'Remise' => 'Remise de prix',
                'Brief' => 'Brief client',
                'Remisereco' => 'Remise de reco',
                'Briefprod' => 'Brief prod'
            );

        elseif($id_entite == '2'):

            $options_type = array(
            'Devis'  =>  'Devis à faire',
            'Crea' =>  'Crea à faire',
            'Prog' =>  'Prog à faire'
            );

        elseif($id_entite == '3'):

            $options_type = array(
            'Devis'  =>  'Devis à faire',
            'Crea' =>  'Crea à faire',
            'Prog' =>  'Prog à faire'
            );
        elseif($id_entite == '4'):

            $options_type = array(
            'Preconisation'  =>  'Préconisation',
            'Mise' =>  'Mise en place',
            'BilanInter' =>  'Bilan intermédiaire',
            'Bilan' => 'Bilan'
            );
        endif;

        
        return $options_type;        
    }

    function cloturer($id)
    {
        return $this->db->delete('taches', array('id' => $id)); 
    }

    function get_historique_list($id_user, $id_entreprise)
    {

        $Now = date('Y-m-d');
        
        $query = $this->db->query('SELECT 
                                    taches.id as t_id,
                                    taches.date as t_date,  
                                    taches.type as t_type, 
                                    taches.commentaire as t_commentaire, 
                                    taches.cloture as t_cloture, 
                                    contacts.nom as c_nom, 
                                    contacts.prenom as c_prenom
                                    FROM `taches` as taches
                                    LEFT JOIN `contacts` AS contacts
                                    ON contacts.id_entreprise = taches.id_entreprise
                                    WHERE taches.date < NOW()
                                    AND taches.id_user ='.$id_user.'
                                    AND taches.id_entreprise ='.$id_entreprise.'
                                    GROUP BY t_id');
                                     
        return $query;
    }

    function get_list_company($id_user)
    {

        $this->db->select('id, nom_societe');
        $this->db->from('entreprises');
        $this->db->where(array('id_user' => $id_user));
        $this->db->order_by('nom_societe', 'ASC'); 

                    
        return $Query = $this->db->get();            
    }

    function get_list_company_virginie()
    {

        $this->db->select('id, nom_societe');
        $this->db->from('entreprises');
        $this->db->where(array('id_entite' => '1'));
        $this->db->order_by('nom_societe', 'ASC'); 

                    
        return $Query = $this->db->get();            
    }

    function get_tache($id_tache)
    {
        $this->db->select('*');
        $this->db->from('taches');
        $this->db->where(array('id' => $id_tache));
                    
        return $Query = $this->db->get();
    }

    function delete($id_tache)
    {
        return $this->db->delete('taches', array('id' => $id_tache)); 
    }
}
