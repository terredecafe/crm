<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('date_us_to_fr'))
{


    function date_us_to_fr($string)
    {
        $temp = explode('-', $string);
        $tmp = explode(' ', $temp[2]);
        $time = @$tmp[1];


        //return $tmp[0].'/'.$temp[1].'/'.$temp[0].' &agrave; ' . $time;
        return $tmp[0].'/'.$temp[1].'/'.$temp[0];
    }

    function date_us_to_fr_with_time($string)
    {
        $temp = explode('-', $string);
        $tmp = explode(' ', $temp[2]);
        $time = @$tmp[1];


        //return $tmp[0].'/'.$temp[1].'/'.$temp[0].' &agrave; ' . $time;
        return $tmp[0].'/'.$temp[1].'/'.$temp[0]. ' - '.$tmp[1];
    }

    function date_fr_to_us($string, $time='')
    {
        $temp = explode('/', $string);


        return @$temp[2].'-'.@$temp[1].'-'.@$temp[0].' ' . @$time;    	
    }

    function get_heure_from_us_date($string)
    {
    	$temp = explode(' ', $string);

    	return $temp[1];
    }
}