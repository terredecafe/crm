<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('body_class'))
{
    function body_class()
    {
        $CI =& get_instance(); // code igniter instance
        
        $class = array();
        
        $class = array(
            $CI->uri->rsegment(1),
            $CI->uri->rsegment(2)
        );
        
        return implode(' ', $class);
    }
}