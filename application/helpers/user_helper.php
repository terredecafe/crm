<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('is_logged_in'))
{
    function user_logged_in()
    {
        $CI =& get_instance();
        $CI->load->library('simplelogin');
        return $CI->simplelogin->is_logged_in();
    }
}
