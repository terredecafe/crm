var baseWebsiteUrl = $('body').data('websiteurl');



//var IdCompany = <?=GET_['id_company']?>;
/**
* Styleswitch stylesheet switcher built on jQuery
* Under an Attribution, Share Alike License
* By Kelvin Luck ( http://www.kelvinluck.com/ )
**/

(function($)
{
	$(document).ready(function() {
	  $('table.stack').stacktable({class:'small-only'});
		$('.nav-bar').on('click', '.delete', function() {
			var answer = confirm("Êtes-vous certain de vouloir supprimer la fiche ?  ");
			if (answer){
				alert("Suppresion de la fiche!");
			}
			else{
				return false;
			}
		})
		
		/*

		$('.styleswitch').click(function()
		{
			switchStylestyle(this.getAttribute("rel"));
			return false;
		});
		var c = readCookie('style');
		if (c !== 'purple') {
		  console.log('ahah');
		}
		
		if (c) {
		  switchStylestyle(c)
		}
		else {
		  $('.styleswitch').removeClass('active');
		  $('.styleswitch[rel=default]').addClass('active');
		}
		*/
        $("#type_filter").live("change keyup", function () {
            $("#filters").submit();
        });

	});

	function switchStylestyle(styleName)
	{
		$('link[rel*=style][title]').each(function(i) 
		{
			this.disabled = true;
			  
			$('.styleswitch').removeClass('active');
			$('.styleswitch[rel='+styleName+']').addClass('active');
			if (this.getAttribute('title') == styleName) this.disabled = false;
		});
		console.log(styleName);
		createCookie('style', styleName, 365);
	}
})(jQuery);
// cookie functions http://www.quirksmode.org/js/cookies.html
function createCookie(name,value,days)
{
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function eraseCookie(name)
{
	createCookie(name,"",-1);
}
// /cookie functions

jQuery.fn.toggleValue = function (index) {
    var that = this;
    that.find("label").eq(index).addClass("ui-state-active").siblings("label").removeClass("ui-state-active");
    console.log(that.parent());
    that.parent().prev('select').find("option").removeAttr('selected').eq(index).attr("selected", true);
    that.parent().find(".ui-slider").slider("value", index * 100);
};

jQuery.fn.toggleSwitch = function (params) {

    var defaults = {
        highlight: true,
        width: 25,
        change: null
    };

    var options = $.extend({}, defaults, params);

    $(this).each(function (i, item) {
        generateToggle(item);
    });

    function generateToggle(selectObj) {

        // create containing element
        var $contain = $("<div />").addClass("ui-toggle-switch");

        // generate labels
        $(selectObj).find("option").each(function (i, item) {
            $contain.append("<label>" + $(item).text() + "</label>");
        }).end().addClass("ui-toggle-switch");

        // generate slider with established options
        var $toggleSlider = $("<div />").slider({
            min: 0,
            max: 100,
            animate: "fast",
            change: options.change,
            stop: function (e, ui) {
                var roundedVal = Math.round(ui.value / 100);
                var self = this;
                window.setTimeout(function () {
                    $(self).toggleValue(roundedVal);
                }, 11);
            },
            range: (options.highlight && !$(selectObj).data("hideHighlight")) ? "max" : null
        }).width(options.width);

        // put slider in the middle
        $toggleSlider.insertAfter(
            $contain.children().eq(0)
		);

        // bind interaction
        $contain.delegate("label", "click", function () {
            if ($(this).hasClass("ui-state-active")) {
                return;
            }
            var labelIndex = ($(this).is(":first-child")) ? 0 : 1;
            $(this).toggleValue(labelIndex);
           	//toggleValue($(this).parent(), labelIndex);
        });

        // initialise selected option
        $contain.find("label").eq(selectObj.selectedIndex).click();

        // add to DOM
        $(selectObj).parent().append($contain);

    }
};

function entrepriseFormatResult(entreprise) {
	console.log(entreprise);
	return entreprise.nom_societe+' - '+entreprise.ville+' ('+entreprise.prenom+' '+entreprise.nom+')';

}

function entrepriseFormatSelection(entreprise) {
	console.log(entreprise);
    return entreprise.nom_societe;
}

function phoningTypeFix() {
    var typePhoning = ['rdv', 'rappel'];
    for (key in typePhoning)
    {
        
        if (typePhoning[key] != $('#type-fld').val()) {
            $('.'+typePhoning[key]+'-content').hide();
        } else {
            $('.'+typePhoning[key]+'-content').show()
        }
    }
}

jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
});

$(document).ready(function() {
    var date = new Date()
    var today = $.datepicker.formatDate('dd/mm/yy', date);

    
    $('.datepicker').each(function() {
        if ($(this).val() == '' || $(this).val() == '//'){
            //$(this).val(today);
        }
    });
    
    $(".datepicker").datepicker();
    
    $(".toggleswitch").toggleSwitch({
    	highlight: true,
    	width: 40
    });
    
    phoningTypeFix();
    $('.phoning').on('change', '#type-fld', function() {
        phoningTypeFix();
    });
    
    $('.ui-select').select2({allowClear:true});
	$('#entreprise').select2({
    formatResultCssClass: function(data) {return undefined;},
    formatNoMatches: function () { return "Aucun résultats"; },
    formatInputTooShort: function (input, min) { return "Veuillez entrer au minimum " + (min - input.length) + " caractères"; },
    formatSelectionTooBig: function (limit) { return "Vous ne pouvez sélectionner que " + limit + " élements"; },
    formatLoadMore: function (pageNumber) { return "Charger plus de résultats..."; },
    formatSearching: function () { return "Recherche en cours..."; },
		placeholder: "Chercher une entreprise",
		minimumInputLength: 3,
		ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
			url: baseWebsiteUrl+"json/entreprises",
			dataType: 'json',
			quietMillis: 100,
			data: function (term) {
				return {
					query: term, // search term
				};
			},
			results: function (data) { // parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to alter remote JSON data
				return {results: data};
			}
		},
		formatResult: entrepriseFormatResult, // omitted for brevity, see the source of this page
		formatSelection: entrepriseFormatSelection, // omitted for brevity, see the source of this page
		dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller
	});
});